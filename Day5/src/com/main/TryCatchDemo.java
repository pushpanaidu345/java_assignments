package com.main;

import java.util.InputMismatchException;
import java.util.Scanner;

/*
 * Java has built-in mechanism to handle exceptions. Using the try statement we can test a block of code for errors. The catch block contains the code that says what to do if exception occurs.  
This problem will test your knowledge on try-catch block. 
You will be given two integers and as input, you have to compute . If and are not bit signed integers or if is zero, exception will occur and you have to report it. Read sample Input/Output to know what to report in case of exceptions. 
Sample Input : 
10 
3 
Sample Output : 
3 
Sample Input : 
10 
Hello 
Sample Output : 
java.util.InputMismatchException 
Sample Input : 
10 
0 
Sample Output : 
java.lang.ArithmeticException: / by zero 
Sample Input : 
23.323 
0 
Sample Output : 
java.util.InputMismatchException 
 

 */
public class TryCatchDemo {

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		System.out.println("enter two numbers:");
		int number1=input.nextInt();
		int number2=input.nextInt();
		try {
			
			int output=number1/number2;
			System.out.println(output);
		}catch(ArithmeticException e) {
			System.out.println(e);
		}catch(InputMismatchException e) {
			System.out.println(e);
		}
	}

}
