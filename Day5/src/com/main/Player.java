package com.main;
/*
 * Write a program to get the name and age of the player from the user and display it.
player name is a string
player age is an integer value
Note : The player is eligible to participate in IPL when their age is 19 and above
This program may generate
1. InvalidAgeRange Custom Exception when the player's age is below 19
Use exception handling mechanisms to handle these exceptions
Create a class called CustomException which extends Exception and it includes constructor to initialize the message.
Use appropriate exception handling mechanisms to handle these exceptions
Sample Input/Output 1:
Enter the player name
Albie Morkel
Enter the player age
35
Player name : Albie Morkel
Player age : 35
Sample Input/Output 2:
Enter the player name
Ishan Kishan
Enter the player age
16
CustomException: InvalidAgeRangeException

 */
import java.util.Scanner;

import com.hcl.InvalidRangeAge;

public class Player  {

	String name;
	int age;

	Player(String name,int age){
	try {
		if(age<19) {
			throw new InvalidRangeAge("customException:InvalidRangeAgeException");}
			else {
				System.out.println("player name: "+name);
			System.out.println("player age: "+age);}
		
		
			
			
		}catch(InvalidRangeAge e) {
			System.out.println(e);//.printStackTrace();
		}
	this.name=name;this.age=age;
}
	
	
	public static void main(String[] args) throws InvalidRangeAge{
		String name;
		 int age;

		Scanner input=new Scanner(System.in);
		System.out.println("enter player name:");
		name=input.next();
		System.out.println("enter player age:");
		age=input.nextInt();
		Player player=new Player(name,age);
			}

}
