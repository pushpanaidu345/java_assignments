package com.main;
/*
 * . Given total runs scored and total overs faced as the input. Write a program to calculate the run rate with the formula,
Run rate= total runs scored/total overs faced.
Use BufferedReader class to get the inputs from user.
This program may generate Arithmetic Exception / NumberFormatException. Use exception handling mechanisms to handle this exception. Use a single catch block. In the catch block, print the class name of the exception thrown.
Sample Input and Output 1:
Enter the total runs scored
79
Enter the total overs faced
14
Current Run Rate : 5.64
Sample Input and Output 2:
Enter the total runs scored
50
Enter the total overs faced
0
java.lang.ArithmeticException
Sample Input and Output 3:
Enter the total runs scored
a
java.lang.NumberFormatException

 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.InputMismatchException;
import java.util.Scanner;

/*
 * Run rate= total runs scored/total overs faced. 
 */
public class CalculateRunrate {
	private int totalRuns;
	private int totalOvers;
	float runrate;
	CalculateRunrate(float totalRuns,float totalOvers)  {
		
		
	try {
		if(totalRuns>0 && totalOvers>0) {
			runrate=totalRuns/totalOvers;
			System.out.println(" Current Run Rate: "+runrate);}
		else if(totalOvers==0)
			throw new ArithmeticException(); 
		else
			throw new InputMismatchException();
	}catch(Exception e) {
		System.out.println(e);
	}
	}
	
	public static void main(String[] args)   {
		float totalRuns;
		float totalOvers;
		//BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the total runs scored ");
		totalRuns=sc.nextInt();
		//totalRuns=(int)br.read();
		//BufferedReader brr=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the total overs faced ");
		totalOvers=sc.nextInt();
		//totalOvers=(int)brr.read();
		//System.out.println(totalRuns+" "+totalOvers);
		CalculateRunrate calculaterunrate =new CalculateRunrate(totalRuns,totalOvers);
		
	}

}
