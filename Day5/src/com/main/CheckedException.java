package com.main;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

/*
 * Handling a checked exception by opening a file 
Write a  code opens a text file and writes its content to the standard output. 
What happens if the file doesn�t exist? 

 */
public class CheckedException {

	public static void main(String[] args) {
		FileReader fopen;
		try {
			fopen = new FileReader("file.doc");
			System.out.println(fopen);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		

	}

}
