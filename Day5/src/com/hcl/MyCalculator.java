package com.hcl;

/*
 * You are required to compute the power of a number by implementing a calculator. Create a class MyCalculator which consists of a single method long power(int, int). This method takes two integers, and , as parameters and finds . If either or is negative, then the method must throw an exception which says "". Also, if both and are zero, then the method must throw an exception which says "" 
For example, -4 and -5 would result in . 
Complete the function power in class MyCalculator and return the appropriate result after the power operation or an appropriate exception as detailed above.  
Sample Input:  
3 5 
2 4 
0 0 
-1 -2 
-1 3 
Sample Output:  
243 
16 
java.lang.Exception: n and p should not be zero. 
java.lang.Exception: n or p should not be negative. 
java.lang.Exception: n or p should not be negative. 

 */
public class MyCalculator {
	public void power(int num1, int num2) {

		long output = 0l;
		try {
			if (num1 > 0 && num2 > 0) {
				output = (long) Math.pow(num1, num2);
			System.out.println(output);}
			else 
				throw new Exception();
			
		} catch (Exception e) {
			if(num1==0 && num2==0)
				System.out.println(e+" :n and p should not be zero.");
			else
				System.out.println(e+" n or p should not be negative. ");
		}
		

	}
}
