package com.main;

import java.util.Scanner;

public class CarMain {

		public static void main(String[] args)  {
		//Connection con = null;

		try {
			
			while (true) {
				System.out.println("Catalog Lists:\n add\n list\n delete\n update\n quit");
				Scanner sc = new Scanner(System.in);

				System.out.print("Enter command: ");
				String command = sc.next();

				switch (command) {
				case "add":
					CarOperations.addCar();
					break;
				case "list":

					CarOperations.listCar();
					break;
				case "delete":
					CarOperations.deleteCar();
					break;
				case "update":
					CarOperations.updateCar();
					break;

				default:
					if (!"quit".equalsIgnoreCase(command)) {
						System.out.println("Sorry, but  " + command + "  is not a valid command. Please try again.");
					}
					break;
				}
				if ("quit".equalsIgnoreCase(command)) {
					System.out.println(" Good Bye");
					break;
				}

			}

		} catch (Exception e) {
			System.out.println(e);
		} 

	}

}
