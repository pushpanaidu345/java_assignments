package com.main;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Scanner;

public class CarOperations {

	public static void addCar() {
		try {
			Scanner sc = new Scanner(System.in);
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = null;
			Statement st = con.createStatement();
			ResultSet rs = null;
			rs = st.executeQuery("select count(*) from car");
			rs.next();
			int countrows = rs.getInt(1);
			if (countrows >= 20) {
				System.out.println("Car table exceeds its size .you are not allowed to add details");
			} else {
				ps = con.prepareStatement("insert into car(make,model,year,saleprice) values(?,?,?,?)");
				
				System.out.print("Make:");
				String make = sc.next();
				System.out.print("Model:");
				String model = sc.next();
				System.out.print("Year:");
				int year = sc.nextInt();
				System.out.print("Sales Price ($) :");
				Float price = sc.nextFloat();
				ps.setString(1, make);
				ps.setString(2, model);

				ps.setInt(3, year);
				ps.setFloat(4, price);
				int count = ps.executeUpdate();
				System.out.println(count + " record details added successfully");
			}
		} catch (Exception e) {
			System.out.println(e);
		}

	}

	public static void listCar() {
		try {
			Scanner sc = new Scanner(System.in);
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = null;
			Statement st;

			st = con.createStatement();

			ResultSet rs = null;
			int sum = 0;
			rs = st.executeQuery("select count(*) from car");
			rs.next();
			int countrows = rs.getInt(1);
			rs = st.executeQuery("select sum(saleprice) from car");
			rs.next();
			sum = rs.getInt(1);

			if (countrows == 0) {
				System.out.println("There are currently no cars in the catalog.");
			} else {

				ps = con.prepareStatement("select *from car");
				rs = ps.executeQuery();
			
				while (rs.next()) {

					System.out.println(String.format("%-10d %-10d %-16s %-16s  %16.2f", rs.getInt(1), rs.getInt(4),
							rs.getString(2), rs.getString(3), rs.getFloat(5)));

				}
				System.out.println("Number of cars:" + countrows);
				System.out.println("Total Inventory: $" + sum);
			}
		} catch (Exception e) {
			System.out.println(e);
		}

	}

	public static void deleteCar() {
		try {
			Scanner sc = new Scanner(System.in);
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = null;
			System.out.println("enter id  u want to delete :");
			int id = sc.nextInt();
			ps = con.prepareStatement("delete  from car where id=?");
			ps.setInt(1, id);
			int delcount = ps.executeUpdate();
			if (delcount == 0) {
				System.out.println("sorry, u entered id is not in car details.Try with other id");
			} else {
				System.out.println("record deleted successfully");
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public static void updateCar() {
		try {
			Scanner sc = new Scanner(System.in);
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = null;

			ps = con.prepareStatement("update  car set model=? where id=?");
			
			System.out.println("enter id you want to update: ");
			int id1 = sc.nextInt();
			System.out.println("enter model ");
			String model1 = sc.next();
			ps.setString(1, model1);
			ps.setInt(2, id1);
			int updatecount = ps.executeUpdate();
			if (updatecount == 0) {
				System.out.println("the row is aleady deleted or it is not present in the table");
			} else {
				System.out.println("record updated successfully");
			}

		} catch (Exception e) {
			System.out.println(e);
		}

	}
}
