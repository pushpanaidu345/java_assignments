package com.service;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dao.PetDao;
import com.dao.UserDao;


@WebServlet("/BuyServlet")
public class BuyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
      PetDao dao=new PetDao();
    UserDao udao=new UserDao();
    ResultSet rs=null;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher=request.getRequestDispatcher("Links.jsp");
		dispatcher.include(request, response);
		HttpSession session=request.getSession();
		int petid=(int) session.getAttribute("petid");
		String username=(String) session.getAttribute("name");
		PrintWriter out=response.getWriter();
		int updatecount=dao.mypet(username,petid);
		System.out.println(petid);
		if(updatecount>0) {
			try {
				rs=udao.getMyPets(username);
				out.println("<style>table{border-collapse:collapse</style>");
				out.println("<table align='center' border='1'>");
				out.println("<tr><th>PetId</th><th>PetName</th><th>PetAge</th><th>PetPlace</th></tr>");
				while(rs.next()) {
					out.println("<tr><td>");
					out.println(rs.getInt(1)+"</td>");
					out.println("<td>");
					out.println(rs.getString(2)+"</td>");
					out.println("<td>");
					out.println(rs.getString(3)+"</td>");
					out.println("<td>");
					out.println(rs.getString(4)+"</td>");
					out.println("</tr>");
				}
				out.println("</table>");
			} catch (Exception e) {
				
				e.printStackTrace();
			}
		}
		  
		 
		}

}
