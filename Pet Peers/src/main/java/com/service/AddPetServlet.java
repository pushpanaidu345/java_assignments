package com.service;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.PetDao;
import com.pojo.Pet;


@WebServlet("/AddPetServlet")
public class AddPetServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       PetDao dao=new PetDao();
      
   
   	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/*
		 * RequestDispatcher dispatcher=request.getRequestDispatcher("Links.jsp");
		 * dispatcher.include(request, response);
		 */
   		
   		String name=request.getParameter("name");
   		int age=Integer.valueOf(request.getParameter("age"));
   		String place=request.getParameter("place");
   		Pet pet=new Pet(name,age,place);
   		int count=dao.addPet(pet);
   		PrintWriter out=response.getWriter();
   		out.println("<h4 style='color:green'>Added successfully</h4>");
   		RequestDispatcher dispatcher=request.getRequestDispatcher("PetService");
		dispatcher.include(request, response);
		
	}

}
