package com.service;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dao.PetDao;


@WebServlet("/PetService")
public class PetService extends HttpServlet {
	private static final long serialVersionUID = 1L;
       PetDao pdao=new PetDao();
    ResultSet rs=null;
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out=response.getWriter();
		RequestDispatcher dispatcher=request.getRequestDispatcher("Links.jsp");
		dispatcher.include(request, response);
		HttpSession session=request.getSession();
		out.println("<br><br>");
		try {
			rs=pdao.getAllPets();
			
			out.println("<style>table{border-collapse:collapse</style>");
			out.println("<table align='center' border='1'>");
			out.println("<tr><th>PetId</th><th>PetName</th><th>PetAge</th><th>PetPlace</th><th>Action</th></tr>");
			
			
			while(rs.next()) {
				/*
				 * int petid=rs.getInt(1); String petname=rs.getString(2); String
				 * petage=rs.getString(3); String petplace=rs.getString(4);
				 */
				out.println("<script> function buy(){document.getElementById('buy').innerHTML='Sold'}</script>");
				int petid=rs.getInt(1);
				session.setAttribute("petid", petid);
				out.println("<tr><td>");
				out.println(rs.getInt(1)+"</td>");
				out.println("<td>");
				out.println(rs.getString(2)+"</td>");
				out.println("<td>");
				out.println(rs.getString(3)+"</td>");
				out.println("<td>");
				out.println(rs.getString(4)+"</td>");
				
				
				/*
				 * out.println("<td><form action='BuyServlet'>");
				 * out.println("<input type='submit'  value='Buy'>");
				 * out.println("</form></td>");
				 */
				
				out.println("<td><a href='BuyServlet' id='buy' onclick=buy()>Buy</a></td>");
				
				out.println("</tr>");
				
			}
			out.println("</table>");
		} catch (Exception e) {
			
			e.printStackTrace();
		}
	}

}
