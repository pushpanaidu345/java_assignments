package com.service;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.UserDao;
import com.pojo.User;


@WebServlet("/UserService")
public class UserService extends HttpServlet {
	private static final long serialVersionUID = 1L;
      UserDao dao=new UserDao();
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	PrintWriter out=response.getWriter();
    	
    	out.println("<h3 style='color:green;background-color:lightgreen;'>You are successfully registered.Please Login</h3>");
    	RequestDispatcher dispatcher=request.getRequestDispatcher("loginPage.jsp");
		dispatcher.include(request, response);
    	}
    
	

}
