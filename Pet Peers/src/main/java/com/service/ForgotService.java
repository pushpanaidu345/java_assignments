package com.service;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.UserDao;


@WebServlet("/ForgotService")
public class ForgotService extends HttpServlet {
	private static final long serialVersionUID = 1L;
       UserDao dao=new UserDao();
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name=request.getParameter("uname");
		String newpassword=request.getParameter("password");
		//String password=request.getParameter("password1");
		PrintWriter out=response.getWriter();
		try {
			int status=dao.changePassword(name,newpassword);
			if(status>0) {
				out.println("<h4 style='color:green;background-color:lightgreen'>password changed successfully</h4>");
				RequestDispatcher rd=request.getRequestDispatcher("loginPage.jsp");
				rd.include(request, response);
			}
		} catch (Exception e) {
			
			e.printStackTrace();
		}
	}

}
