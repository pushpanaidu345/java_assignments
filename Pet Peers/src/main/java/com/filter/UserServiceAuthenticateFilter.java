package com.filter;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpSession;

import com.dao.UserDao;


@WebFilter("/UserServiceAuthenticate")
public class UserServiceAuthenticateFilter implements Filter {
	UserDao dao=new UserDao();
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		String name=request.getParameter("uname");
		String password=request.getParameter("upassword");
		
		PrintWriter out=response.getWriter();
		boolean status=dao.authenticateUser( name, password);
		if(status) {
			
			chain.doFilter(request, response);
		}
		else {
			out.println("<h4 style='color:red'>username or password or both are invalid</h4>");
			RequestDispatcher dispatcher=request.getRequestDispatcher("loginPage.jsp");
			dispatcher.include(request, response);
		}
		
	}

	
	public void init(FilterConfig fConfig) throws ServletException {
		
	}

	@Override
	public void destroy() {
		
		
	}

}
