package com.filter;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

import com.dao.UserDao;
import com.pojo.User;


@WebFilter("/UserService")
public class UserServiceFilter implements Filter {
	UserDao dao=new UserDao();
    
	public void destroy() {
		
	}

	
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		String name=request.getParameter("name");
    	String password=request.getParameter("password");
    	PrintWriter out=response.getWriter();
    	User user=new User(name,password);
    	int count;
		try {
			count = dao.saveUser(user);
			if(count>0) {
				chain.doFilter(request, response);
			}
	    	else {
	    		out.println("<h3 style='color:red'>username should be unique</h3>");
	    		RequestDispatcher dispatcher=request.getRequestDispatcher("registrationPage.jsp");
	    		dispatcher.include(request, response);
	    	}
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
    	
		
	}

		public void init(FilterConfig fConfig) throws ServletException {
		
	}

}
