package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import com.dbconnection.DBConnection;
import com.pojo.Pet;

public class PetDao {
	Connection con=null;
	PreparedStatement ps=null;
	ResultSet rs=null;
Statement st=null;
	public ResultSet getAllPets() throws Exception {
		con = DBConnection.getConnection();
		 ps=con.prepareStatement("select petid,petname,petage,petplace from pet");
		return rs=  ps.executeQuery();
		 
		
}
	public int addPet(Pet pet) {
		int count=0;
		try {
			con = DBConnection.getConnection();
		 ps=con.prepareStatement("insert into pet(petname,petage,petplace)values('"+pet.getPetname()+"','"+pet.getPetage()+"','"+pet.getPetplace()+"')");
		 count= ps.executeUpdate();
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		return count;
		
	}
	public int mypet(String username,int petid) {
		int updatecount=0;
		try {
			con = DBConnection.getConnection();
			st=con.createStatement();
			 rs=st.executeQuery("select userid from petuser where username='"+username+"'");
			 rs.next();
			 int id=rs.getInt("userid");
			 ps=con.prepareStatement("update pet set petownerid='"+id+"' where petid='"+petid+"' ");
			  updatecount=ps.executeUpdate();
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		return updatecount;
		
	}
}
