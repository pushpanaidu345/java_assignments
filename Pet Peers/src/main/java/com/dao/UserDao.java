package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.dbconnection.DBConnection;
import com.pojo.User;

public class UserDao {
	Connection con=null;
	PreparedStatement ps=null;
	ResultSet rs=null;
	Statement st=null;
	int count;
	public  int saveUser(User user) throws SQLException {
		
		try {
			
			 con = DBConnection.getConnection();
			 ps=con.prepareStatement("insert into petuser(username,userpassword) values('"+user.getName()+"','"+user.getPassword()+"')");
			  count=ps.executeUpdate();
			//System.out.println(count);
			 
		}catch(Exception e) {
			
		}
		finally {
			ps.close();
			con.close();
		}
		return count;
	}
	public boolean authenticateUser(String name,String password) {
		boolean status=false;
	
		try {
			
			 con = DBConnection.getConnection();
			 PreparedStatement ps=con.prepareStatement("select *from petuser where username='"+name+"' and userpassword='"+password+"'");
			 rs=ps.executeQuery();
			 status=rs.next();
	}catch(Exception e) {
		System.out.println(e);
	}
		return status;
	}
	public ResultSet getMyPets(String username) throws Exception {
		 
			con = DBConnection.getConnection();
			st=con.createStatement();
			 rs=st.executeQuery("select userid from petuser where username='"+username+"'");
			 rs.next();
			 int id=rs.getInt("userid");
			PreparedStatement ps=con.prepareStatement(" select petid,petname,petage,petplace from pet where petownerid='"+ id+"' ");
			return ps.executeQuery();
		
			
			
			}
	public int changePassword(String name,String newpassword) throws Exception {
		con = DBConnection.getConnection();
		PreparedStatement ps=con.prepareStatement("update petuser set userpassword='"+newpassword+"' where username='"+name+"'");
		int status=ps.executeUpdate();
		return status;
		
	}
}
