package com.pojo;

public class Pet {
	private String petname;
	private int petage;
	private String petplace;
	public Pet(String petname, int petage, String petplace) {
		super();
		this.petname = petname;
		this.petage = petage;
		this.petplace = petplace;
	}
	public String getPetname() {
		return petname;
	}
	public void setPetname(String petname) {
		this.petname = petname;
	}
	public int getPetage() {
		return petage;
	}
	public void setPetage(int petage) {
		this.petage = petage;
	}
	public String getPetplace() {
		return petplace;
	}
	public void setPetplace(String petplace) {
		this.petplace = petplace;
	}
	
	

}
