<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style>
div {
	background-color:tomato ;
	
}

input[type="submit"], input[type="reset"] {
	background-color: blue;
}

table {
	border-collapse: collapse;
}
</style>
</head>
<body>
	<div class="shop">
		<strong>Pet Shop</strong>&nbsp;&nbsp; 
		<a href="PetService" style="color:blank">Home</a>&nbsp;&nbsp;
		 <a href="myPetsPage">MyPet</a>&nbsp;&nbsp;
		<a href="addPetPage.jsp">Add Pet</a>&nbsp;&nbsp;
		 <a href="logoutPage" style="position: relative; left: 520px;">LogOut</a>
	</div>
	<br>
	<br>

	<h3 align="center">Pet Information</h3>

	<form action="AddPetServlet">
		<table align="center" border="1">

			<tr>
				<td>Name</td>
				<td><input type="text" name="name"></td>
			</tr>
			<tr>
				<td>Age</td>
				<td><input type="text" name="age"></td>
			</tr>
			<tr>
				<td>Place</td>
				<td><input type="text" name="place"></td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" value="Add Pet">&nbsp;&nbsp;&nbsp;
					<input type="reset" value="Cancel"></td>
			</tr>
		</table>
	</form>
</body>
</html>