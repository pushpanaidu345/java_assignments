package com.main;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SelectRecord {

	public static void main(String[] args) throws SQLException {
		Connection con=null;
		Statement st=null;
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con=DriverManager.getConnection("jdbc:mysql://localhost:3306/practice","root","root");
			st=con.createStatement();
			ResultSet rs=st.executeQuery("select *from users");
			while(rs.next()) {
				System.out.print("Username: "+rs.getString(1));
				System.out.print("\tPassword: "+rs.getString(2));
				System.out.print("\tfullname: "+rs.getString(3));

				System.out.print("\temail: "+rs.getString(4));
				System.out.println("\n");
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		finally {
			st.close();
			con.close();
		}
		
		}

}
