package com.main;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DeleteRecord {

	public static void main(String[] args) throws SQLException {
		Connection con=null;
		Statement st=null;
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con=DriverManager.getConnection("jdbc:mysql://localhost:3306/practice","root","root");
			st=con.createStatement();
			int count=st.executeUpdate("delete from users where username='steve'");
			System.out.println(count+" row are deleted");
		} catch (Exception e) {
			System.out.println(e);
		}
		finally {
			st.close();
			con.close();
		}
	}

}
