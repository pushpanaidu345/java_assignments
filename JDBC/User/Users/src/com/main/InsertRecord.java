package com.main;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class InsertRecord {

	public static void main(String[] args) throws SQLException {
		Connection con=null;
				try
	    {
	       Scanner sc=new Scanner(System.in);
	       System.out.println("enter username:");
	       String username=sc.next();
	       System.out.println("enter password:");
	       String password=sc.next();
	       System.out.println("enter fullname:");
	       String fullname=sc.next();
	       System.out.println("enter email:");
	       String email=sc.next();
	        Class.forName("com.mysql.cj.jdbc.Driver");
	   
	    con=DriverManager.getConnection("jdbc:mysql://localhost:3306/practice","root","root");
	    PreparedStatement ps=con.prepareCall("insert into users values(?,?,?,?)");
	    ps.setString(1,username);
	    ps.setString(2, password);
	    ps.setString(3, fullname);
	    ps.setString(4, email);
	    int count=ps.executeUpdate();
	    System.out.println(count+ "rows are added");
	    
	    }catch(Exception e)
	    {
	        System.out.println(e);
	    }
	    finally
	    {
	       
	        con.close();
	    }

	}

}
