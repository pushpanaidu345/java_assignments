package com.main;

class Cycle {
	String define_me() {
		return "a vehicle with pedals.";
	}
}

class Motor extends Cycle {
	String define_me() {
		return "I am cycle with an engine.";
	}

	Motor() {
		System.out.println("Hello I am a Motorcycle " + define_me());
		String temp = super.define_me();
		System.out.println("My ancestor is cycle who is " + temp);
	}
}

public class InheritanceExample {

	public static void main(String[] args) {
		Motor M = new Motor();

	}

}
