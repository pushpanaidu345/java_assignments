package com.main;

import com.hcl.Room;

public class RoomMain {

	public static void main(String[] args) {
		Room room = new Room();
		room.setRoomno(100);
		room.setRoomarea(25000);
		room.setRoomtype("flat");
		room.setAcmachine("HP");
		room.displayData();
	}

}
