package com.hcl;

/*
 * print sum as given in the sample output
 * input: 1 2 3 4 5 6 
 * output:
 * 1+2=3
 * 1+2+3=6
 * 1+2+3+4=10
 * 1+2+3+4+5=15
 * 1+2+3+4+5+6=21
 */
public class Addition {
	public void add(int[] arrayinput) {
		int sum=0;
		sum=arrayinput[0];
		
		
		
		for (int i = 1; i < arrayinput.length; i++) {
			sum=sum+arrayinput[i];
			System.out.println(i+"+"+(i+1)+"="+sum);
			
			/*
			 * if(i==arrayinput.length) sum=sum+arrayinput[arrayinput.length];
			 */
			
		}
	}

}
