package com.hcl;

/*
 * display middle character of a string
 * if length is odd there will be 1 character
 * if length is even there will be 2 character
 */
public class FindMiddleChar {
	public String toFindCharacter(String input) {
		int length = input.length();
		int middleindex = length / 2;
		String output = "";
		if (length % 2 != 0)
			output = output + input.charAt(middleindex);
		else
			output = output + input.substring(middleindex - 1, middleindex + 1);
		return output;
	}

}
