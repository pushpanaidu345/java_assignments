package com.hcl;

/*
 * find smallest number among three numbers
 */
public class FindSmallNumber {
	public int findSmall(int input1, int input2, int input3) {
		int output = Math.min(input1, Math.min(input2, input3));
		return output;
	}
}
