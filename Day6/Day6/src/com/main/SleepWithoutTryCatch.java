package com.main;
/*
 * 2 	In the previous program remove the try{}catch(){} block surrounding 
 * the sleep method and try to execute the code. What is your observation? 
 

 */
import java.time.LocalTime;

class threadclass1 extends Thread {
	public void run() {

		System.out.println(Thread.currentThread().getName() + " running");
		LocalTime time = LocalTime.now();
		System.out.println("current time " + time);

		 Thread.sleep(10000); 	//without try/catch we cannot define sleep()
									// because it does not handle InterruptedException

		System.out.println("current time after 10sec  " + time);
	}
}

public class SleepWithoutTryCatch {

	public static void main(String[] args) {
		threadclass1 t = new threadclass1();
		// t.start();
		threadclass1 t1 = new threadclass1();
		t1 = t;
		t1.setName("MyThread");
		t1.start();

	}

}
