package com.main;

import java.time.LocalTime;

/*
 * Write a program to assign the current thread to t1. 
 * Change the name of the thread to MyThread.
 *  Display the changed name of the thread. 
 *  Also it should display the current time. 
 *  Put the thread to sleep for 10 seconds and display the time again. 
 */
 class threadclass extends Thread{
	 public void run() {
		
		 System.out.println(Thread.currentThread().getName() +" running");
		 LocalTime time=LocalTime.now();
		 System.out.println("current time "+time);
		 try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
		
		 System.out.println("current time after 10sec  "+time);
	 }

	
 }
public class SimpleThread {

	public static void main(String[] args) {
		threadclass t=new threadclass();
		//t.start();
		threadclass t1=new threadclass();
		t1=t;
		t1.setName("MyThread");
		t1.start();
		

	}

}
