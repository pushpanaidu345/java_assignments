package com.main;
/*
 * Rewrite the earlier program so that, now the class DemoThread1 instead of implementing from Runnable interface, will now extend from Thread class. 
 */
public class ConstructorThread  extends Thread{
	ConstructorThread( ){
		Thread thread=new Thread(); 
		thread.start();    // no output. start() is in main method only
		
	}
	
	public static void main(String[] args) {
		ConstructorThread t1=new ConstructorThread();  
		ConstructorThread t2=new ConstructorThread();
		ConstructorThread t3=new ConstructorThread();
		t1.start();  //this is the right way to call start()
		t2.start();
		t3.start();
	}

	@Override
	public void run() {
		for(int i=0;i<10;i++) {
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
			System.out.println("running child Thread in loop : "+i);
		}
		
	}

}
