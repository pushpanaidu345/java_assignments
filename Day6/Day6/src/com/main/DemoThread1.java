package com.main;
/*
 * 3 	Write a program to create a class DemoThread1 implementing Runnable interface. 
 * In the constructor, create a new thread and start the thread. 
 * In run() display a message "running child Thread in loop : 
 * " display the value of the counter ranging from 1 to 10. 
 * Within the loop put the thread to sleep for 2 seconds.
 *  In main create 3 objects of the DemoTread1 and execute the program. 
 

 */
public class DemoThread1 implements Runnable{
	DemoThread1( ){
		Thread thread=new Thread();
		thread.start();
		
	}
	
	public static void main(String[] args) {
		DemoThread1 t1=new DemoThread1();  //no output because runnable interface has only one method
											//that is run() if we want invoke start() by using instance of Thread 
		DemoThread1 t2=new DemoThread1();   //start() is in main() method only
		DemoThread1 t3=new DemoThread1();
	}

	@Override
	public void run() {
		for(int i=0;i<10;i++) {
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
			System.out.println("running child Thread in loop : "+i);
		}
		
	}

}
