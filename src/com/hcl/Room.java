package com.hcl;
/*
 * to display roomno,roomtype,roomarea,acmachine.
 */
public class Room {
private int roomno;
private String roomtype;
private long roomarea;
private String acmachine;
public int getRoomno() {
	return roomno;
}
public void setRoomno(int roomno) {
	this.roomno = roomno;
}
public String getRoomtype() {
	return roomtype;
}
public void setRoomtype(String roomtype) {
	this.roomtype = roomtype;
}
public long getRoomarea() {
	return roomarea;
}
public void setRoomarea(long roomarea) {
	this.roomarea = roomarea;
}
public String getAcmachine() {
	return acmachine;
}
public void setAcmachine(String acmachine) {
	this.acmachine = acmachine;
}
public void displayData()
{
	System.out.println("Room Details: ");
	System.out.println("Room Number "+getRoomno());
	System.out.println("Room Type "+getRoomtype());
	System.out.println("Room Area "+getRoomarea());
	System.out.println("AcMachine "+getAcmachine());
	
}
}
