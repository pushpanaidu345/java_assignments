package com.main;

import java.util.Scanner;

import com.hcl.FindMiddleChar;

public class MiddleCharacterMain {

	public static void main(String[] args) {
		FindMiddleChar middlechar = new FindMiddleChar();
		Scanner sc=new Scanner(System.in);
		System.out.println("enter string:");
		String str=sc.next();
		//System.out.println("for odd length  " + middlechar.toFindCharacter("367"));
		System.out.println("middle char in  string  " + middlechar.toFindCharacter(str));

	}

}
