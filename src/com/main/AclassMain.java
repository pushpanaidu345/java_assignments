package com.main;
/*
 * create class named as 'A' and craete a sub class 'B'.
 * which is extends from class 'A'.
 * And use these classes in 'inherit' class.
 */

class A {
	public void display() {
		System.out.println(" Hey! I am parent class");
	}
}

class B extends A {
	public void display1() {
		System.out.println(" Hey! I am child class");
	}

}

public class AclassMain {

	public static void main(String[] args) {
		A a = new A();
		a.display();
		B b = new B();
		b.display();
		b.display1();

	}

}
