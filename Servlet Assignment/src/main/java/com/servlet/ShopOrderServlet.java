package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/ShopOrderServlet")
public class ShopOrderServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name=request.getParameter("uname");
		String email=request.getParameter("email");
		String phone =request.getParameter("phone");
		PrintWriter out=response.getWriter();
		out.println("<h2>YAEBS-Order Confirmation</h2><br>");
		out.println("Customer Name:         "+name+"<br>");
		out.println("Customer email:        "+email+"<br>");
		out.println("Customer Phone Number: "+phone+"<br>");
		//String[] books=request.getParameterValues("details");
		
		
		
		
		out.println("<h4>Thank You</h4><br>");
		out.println("<a href='ShopServlet'>BACK TO SELECT MENU</a>");
	}

}
