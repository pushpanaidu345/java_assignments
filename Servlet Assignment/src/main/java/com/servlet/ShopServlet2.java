package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.service.LoginService;


@WebServlet("/ShopServlet2")
public class ShopServlet2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	PrintWriter out=response.getWriter();
	String name=request.getParameter("name");
	String title=request.getParameter("title");
	//LoginService.check(name);
	HttpSession session=request.getSession();
	session.setAttribute("name", name);
	session.setAttribute("title", title);
	response.sendRedirect("ShopService");
	}

}
