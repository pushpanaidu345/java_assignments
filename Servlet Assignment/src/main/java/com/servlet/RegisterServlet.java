package com.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.service.RegisterServiceImpl;


@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	String name=request.getParameter("name");
	String password=request.getParameter("password");
	String email=request.getParameter("email");
	String mobile=request.getParameter("mobile");
	String dob=request.getParameter("dob");
	String gender=request.getParameter("gender");
	String country=request.getParameter("country");
	RegisterServiceImpl service=new RegisterServiceImpl();
	service.addDetails(name,email,mobile,dob,gender,country);
   	}

}
