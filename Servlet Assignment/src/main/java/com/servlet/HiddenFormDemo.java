package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/HiddenFormDemo")
public class HiddenFormDemo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
    public HiddenFormDemo() {
            }

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out=response.getWriter();
		//response.setContentType("Html/text");
		String uname=request.getParameter("name");
		
		out.println("<form action='HiddenFormDemo2'>");
		out.println("<input type='hidden' name='name' value='"+uname+"'>");
		out.println("<input type='submit' value='submit'>");
		out.println("</form>");
	}

	

}
