package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


//@WebServlet("/ServletConfig")
public class ServletConfig extends HttpServlet {
	private static final long serialVersionUID = 1L;
    	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out=response.getWriter();
		/*Enumeration<String> en=request.getParameterNames();
		int sum=0;
		while(en.hasMoreElements()) {
			String param=en.nextElement();
			String value=request.getServletContext().getInitParameter(param);
			int num=Integer.parseInt(value);
			out.println("Number Value is: "+num);
			sum=sum+num;
			
		}
		out.println("Result is:"+sum);
    	}*/
		String n1=getInitParameter("n1");
		String n2=getInitParameter("n2");
		out.println("Number1 value is:"+n1+"<br>");
		out.println("Number2 value is:"+n2+"<br>");
		int sum=Integer.parseInt(n1)+Integer.parseInt(n2);
		out.println("Result  is:"+sum);
    	}

}
