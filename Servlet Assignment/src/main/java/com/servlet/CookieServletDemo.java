package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/CookieServletDemo")
public class CookieServletDemo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	PrintWriter out=response.getWriter();
	response.setContentType("text/html");
	String uname=request.getParameter("name");
	//Cookie cookie=new Cookie("name",uname);
	
    out.println("Welcome"+uname);
    String encodecookie=URLEncoder.encode(uname,"UTF-8");
    Cookie cookie=new Cookie("name",encodecookie);
   
    response.addCookie(cookie);
    
	out.println("<form action='CookieServletDemo2'>");
	out.println("<input type='submit' value='submit'>");
   	}

}
