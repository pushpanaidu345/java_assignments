package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/SessionRegisterServlet")
public class SessionRegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		out.println("<form action='submit.html'>");
		out.println("Name:<input type='text'><br>");
		out.println("Password:<input type='password'><br>");
		out.println("<input type='submit' value='Register'>");
		out.println("</form>");
		request.getRequestDispatcher("Links.html").include(request, response); 
		
		 
	}

}
