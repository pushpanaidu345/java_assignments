package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/HttpSessionServlet")
public class HttpSessionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out=response.getWriter();
    		String name=request.getParameter("name");
		String password=request.getParameter("password");
		HttpSession session=request.getSession();
		session.setAttribute("name",name);
		out.println("<a href='HttpSessionServlet2'>Session2</a>");
		
	}

}
