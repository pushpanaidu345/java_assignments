package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/ShopServlet")
public class ShopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
   	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out=response.getWriter();
		response.setContentType("text/html");
		out.println("<h3>Welcome to Yet Another E-BookShop</h3><br>");
		out.println("<form action='ShopServlet2'>");
		out.println("Author Name:<select name='name'>");
		out.println("<option>select..</option>");
		out.println("<option>Valmiki</option>");
		out.println("<option>vemana</option>");
		out.println("<option>samantha</option><br>");
		out.println("</select>");
		out.println("<h3>or</h3>");
		out.println("Search Title:<input type='text' name='title'><br>");
		out.println("<input type='submit'value='Search'>");
		out.println("</form>");
	}

}
