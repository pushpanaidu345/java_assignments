package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/DBConnectionServlet")
public class DBConnectionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out=response.getWriter();
    	ServletContext context=getServletContext();
		String name=request.getParameter("name");
		String userpassword=request.getParameter("password");
		
		String url=context.getInitParameter("Url");
		String username=context.getInitParameter("username");
		String password=context.getInitParameter("password");
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			
			
			Connection con=DriverManager.getConnection(url,username,password);
			out.println("connection connected<br>");
			PreparedStatement ps=con.prepareStatement("insert into login values('"+name+"','"+userpassword+"')");
			int count=ps.executeUpdate();
			out.println(count+" added successfully");
		}catch(Exception e) {
			out.println(e);
					}
    	}

}
