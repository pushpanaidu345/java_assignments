package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/*Write a servlet that refreshes the browser every second and 
 * current time will be changed automatically.*/
@WebServlet("/ServletCalender")
public class ServletCalender extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			
		response.setIntHeader("Refresh", 1);
		Calendar calendar=new GregorianCalendar(); 
			PrintWriter out=response.getWriter();
			out.println(calendar.getTime());
	}

}
