package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 
 */
@WebServlet("/SessionTracking")
public class SessionTracking extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession();
		 Date lasttime = new Date(session.getLastAccessedTime());
		 Date creationtime = new Date(session.getCreationTime());

		   String title = "Welcome Back to my website";
		   int visitCount;
		  
		  
		   String userID = request.getParameter("userid");
		  

		  
		   if (session.isNew() ){
		      title = "Welcome to my website";
		     visitCount=0;
		     session.setAttribute("userIDKey", userID);
			   session.setAttribute("visitCountKey",  visitCount);
		   } 
		   else {
		   visitCount = (Integer)session.getAttribute("visitCountKey");
		   visitCount = visitCount + 1;
		   userID = (String)session.getAttribute("userIDKey");
		   session.setAttribute("visitCountKey",  visitCount);
		   }

		PrintWriter out=response.getWriter();
		response.setContentType("text/html");
		out.println(title+"<br>");
		out.println("<b>Session Information<b><br>");
		out.println("<table border='1'>");
		out.println("<tr><th>Sessio Info</th><th>Value</th></tr>");
		out.println("<tr><td>id</td><td>");
		out.println(session.getId());
		out.println("</td></tr>");
		out.println("<tr><td>Creation Time</td><td>");
		out.println(creationtime);
		out.println("</td></tr>");
		out.println("<tr><td> Time of Last Access</td><td>");
		out.println(lasttime);
		out.println("</td></tr>");
		out.println("<tr><td> UserId</td><td>");
		out.println(session.getAttribute("userIDKey"));
		out.println("</td></tr>");
		out.println("<tr><td>Number Of Visits</td><td>");
		out.println(session.getAttribute("visitCountKey"));
		out.println("</td></tr>");
		out.println("</table>");
		}

}
