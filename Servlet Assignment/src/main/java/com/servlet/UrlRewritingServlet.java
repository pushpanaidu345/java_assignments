package com.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/UrlRewritingServlet")
public class UrlRewritingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    		String name=request.getParameter("name");
    		String password=request.getParameter("password");
    		if(password.equals("admin")) {
    			response.sendRedirect("UrlRewritingServlet2?username="+name);
    		}
    	}

}
