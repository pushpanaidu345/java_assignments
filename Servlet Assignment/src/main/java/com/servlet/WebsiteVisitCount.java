package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 4) Write a Servlet application to count the total number of visits on the
 * website.
 */
@WebServlet("/WebsiteVisitCount")
public class WebsiteVisitCount extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		int count;
		if (session.isNew()) {

			count = 0;

			session.setAttribute("count", count);
		} else {
			count = (Integer) session.getAttribute("count");
			count = count + 1;

			session.setAttribute("count", count);
		}

		PrintWriter out = response.getWriter();
		out.println("Number of visits " + count);

	}

}
