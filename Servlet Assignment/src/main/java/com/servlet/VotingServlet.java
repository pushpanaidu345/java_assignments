package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 5) Write a Servlet program that accepts the age and name and 
 * displays if the user is eligible for voting or not.
 */
@WebServlet("/VotingServlet")
public class VotingServlet extends HttpServlet {
	
	
	 
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out =response.getWriter();
		String name=request.getParameter("name");
		String age1=request.getParameter("age");
		int age=Integer.parseInt(age1);
		if(age<18) {
			out.println(name+" is not eligible for vote");
		}
		else
			out.println(name+" is eligible for vote");
	}

}
