package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/HiddenFormDemo2")
public class HiddenFormDemo2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
        public HiddenFormDemo2() {
            }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username=request.getParameter("name");
		PrintWriter out =response.getWriter();
		out.println("Welcome "+username);
	}

}
