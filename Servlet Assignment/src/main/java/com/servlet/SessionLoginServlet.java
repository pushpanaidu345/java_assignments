package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.service.LoginService;


@WebServlet("/SessionLoginServlet")
public class SessionLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");  
        PrintWriter out=response.getWriter(); 
        HttpSession session=request.getSession();
               String name=request.getParameter("name");  
        String password=request.getParameter("password"); 
        out.print("Welcome, "+name+"<br>");  
        request.getRequestDispatcher("Links.html").include(request, response);  
          session.setAttribute("name", name);
        
		/*
		 * if(LoginService.validate(name, password)) {
		 * 
		 * 
		 * session.setAttribute("name",name); } else{
		 * out.print("Sorry, username or password error!");
		 * request.getRequestDispatcher("SessionLogin.html").include(request, response);
		 * }
		 */
       session.setMaxInactiveInterval(5);
        
        }
	

}
