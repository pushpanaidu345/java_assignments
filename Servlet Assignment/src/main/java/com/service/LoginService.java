package com.service;

import com.dao.LoginDao;

public class LoginService {
	public static boolean validate(String name,String password) {
		return LoginDao.validate(name, password);
	}
	public static void check(String name) {
		LoginDao.check(name);
	}

}
