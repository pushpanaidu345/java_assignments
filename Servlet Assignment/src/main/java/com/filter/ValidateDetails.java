package com.filter;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

import com.service.LoginService;


@WebFilter("/ValidateServlet")
public class ValidateDetails implements Filter {

   
    public ValidateDetails() {
           }

		public void destroy() {
		
	}

		public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		String name=request.getParameter("name");
		String password=request.getParameter("password");
		PrintWriter out=response.getWriter();
		if(LoginService.validate(name, password)) {
		chain.doFilter(request, response);}
		else if(name==" "||password==" ") {
			out.println("Please enter username and password");
			RequestDispatcher dispatcher=request.getRequestDispatcher("LoginValid.html");
			dispatcher.include(request, response);
		}
		else {
			out.println("<p style='color:red'>Wrong username or password</p>");
			RequestDispatcher dispatcher=request.getRequestDispatcher("LoginValid.html");
			dispatcher.include(request, response);
		}
	}

		@Override
		public void init(FilterConfig arg0) throws ServletException {
			
			
		}

		
}
