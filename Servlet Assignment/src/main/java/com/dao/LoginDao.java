package com.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.dbconnection.DBConnection;

public class LoginDao {
	 
	public static boolean validate(String name,String password) {
		boolean status=false;
		try {
			
			Connection con=DBConnection.getConnection();
			PreparedStatement ps=con.prepareStatement("select * from login where name=? and password=?");  
		  
		ps.setString(1,name);  
		ps.setString(2,password);  
		      
		ResultSet rs=ps.executeQuery();  
		status=rs.next();  
		}catch(Exception e) {
			System.out.println(e);
		}
		
		return status;
	}
	public static void check(String name) {
		try {
			Connection con=DBConnection.getConnection();
			PreparedStatement ps=con.prepareStatement("select *from authordetails where author='"+name+"'");
			ResultSet rs=ps.executeQuery();
			
			System.out.println("Author\t\tTitle\t\tPrice");
			while(rs.next()) {
				System.out.format("%-10s %-15s %-15f",rs.getString(1),rs.getString(2),rs.getFloat(3));
			}
		} catch (Exception e) {
			
			e.printStackTrace();
		}
	}

}
