package com.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class RegisterDaoImpl implements RegisterDao{

	@Override
	public void addDetails(String name,String email,String mobile,String dob,String gender,String country) {
	
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/practice","root","root");
			PreparedStatement ps=con.prepareStatement("insert into registration(first_name,email,mobile,dob,gender,country)values(?,?,?,?,?,?)");
			ps.setString(1, name);
			ps.setString(2, email);
			ps.setString(3, mobile);
			ps.setString(4, dob);
			ps.setString(5, gender);
			ps.setString(6, country);
			int count=ps.executeUpdate();
			System.out.println(count+"added successfully ");		}catch(Exception e) {
			System.out.println(e);
		}
		
	}

}
