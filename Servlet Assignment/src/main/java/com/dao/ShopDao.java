package com.dao;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/ShopDao")
public class ShopDao extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out=response.getWriter();
    	ServletContext context=getServletContext();
    	HttpSession session=request.getSession();
		String name=(String) session.getAttribute("name");
		String title=(String) session.getAttribute("title");
		
		
		String url=context.getInitParameter("Url");
		String username=context.getInitParameter("username");
		String password=context.getInitParameter("password");
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			
			
			Connection con=DriverManager.getConnection(url,username,password);
			PreparedStatement ps=con.prepareStatement("select *from authordetails where author='"+name+"' or  title='"+title+"'");
			ResultSet rs=ps.executeQuery();
			out.println("<h2>YAEBS-Query Results</h2>");
			out.println("<style>table{border-collapse:collapse}</style>");
			out.println("<table border='1'>");
			out.println("<tr><th></th><th>Author   </th><th>Title   </th><th>Price   </th><th>Quantity  </th></tr>");
			
			
			while(rs.next()) {
				String authorname=rs.getString(1);
				String titlename=rs.getString(2);
				int price=rs.getInt(3);
				int quantity=rs.getInt(4);
				String value=authorname+titlename+price;
				//session.setAttribute("authorname", authorname);
				//session.setAttribute("titlename", titlename);
				//session.setAttribute("price", price);
				//response.sendRedirect("ShopResultServlet");
				
				//out.println("<tr><th></th><th>Author</th><th>Title</th><th>Price</th><th>Quantity</th></tr>");
				out.println("<tr><td>");
				out.println("<input type='checkbox' name='details' value='"+value+"'></td>");
				out.println("<td>");
				out.println(authorname+"</td>");
				out.println("<td>");
				out.println(titlename+"</td>");
				out.println("<td>");
				out.println(price+"</td>");
				out.println("<td>");
				//out.println(quantity+"</td><tr>");
				out.println("<input type='number' name='qua'></tr>");
				
				//out.println(String.format("%-10s %-15s %-15f",rs.getString(1),rs.getString(2),rs.getFloat(3))+"<br>");
				
				
			}
			out.println("</table>");
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		out.println("<br><br>");
		out.println("<form action='ShopOrderServlet'>");
		out.println("Enter your Name:<input type='text'name='uname'><br>");
		out.println("Enter your Email:<input type='email'name='email'><br>");
		out.println("Enter your phone number:<input type='tel' pattern='[0-9]{10}' name='phone'><br>");
		out.println("<input type='submit' value='ORDER'>&nbsp;&nbsp;&nbsp;&nbsp;");
		out.println("<input type='reset' value='CLEAR'>");
		out.println("<br><br>");
		out.println("<a href='ShopServlet'>BACK TO SELECT MENU</a>");

	}

}
