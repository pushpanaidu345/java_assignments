package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

 

import org.hibernate.validator.constraints.NotEmpty;
//package com.model;
 

@Entity
@Table(name="Patient_info")
public class Patient {
     @Id
     @GeneratedValue(strategy=GenerationType.IDENTITY)
     private int patientid;
     //@NotNull(message="Firstname is required")
     private String firstname;
    // @NotEmpty(message="Lastname is required")
      private String lastname;
     //@NotEmpty(message="Age is required")
      private int age;
     //@NotEmpty(message="Gender is required")
      private String gender;
     //@NotEmpty(message="Address is required")
      private String address;
     //@NotEmpty(message="Mobile Number atleast 10 characters")
      private String mobileno;

 

    public int getPatientid() {
    return patientid;
      }
     public void setPatientid(int patientid) {
    this.patientid = patientid;
     }
    public String getFirstname() {
        return firstname;
    }
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }
    public String getLastname() {
        return lastname;
    }
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public String getGender() {
        return gender;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public String getMobileno() {
        return mobileno;
    }
    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    
    } 
    

 

}