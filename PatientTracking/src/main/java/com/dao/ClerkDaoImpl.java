package com.dao;

import java.util.List;

<<<<<<< HEAD
import org.hibernate.Query;
import org.hibernate.SQLQuery;
=======
>>>>>>> 5e448212e5eb21d89de2a3d35a2c6e369401553f
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.model.Clerk;
<<<<<<< HEAD
import com.model.Doctor;
=======
>>>>>>> 5e448212e5eb21d89de2a3d35a2c6e369401553f
@Repository
public class ClerkDaoImpl implements ClerkDaoIntf{
	@Autowired
	SessionFactory sessionFactory;
	public void saveClerk(Clerk clerk) {
		sessionFactory.openSession().save(clerk);
		
	}
	
	public List<Clerk> clerkDetails() {
		List<Clerk> li=(List<Clerk>) sessionFactory.openSession().createQuery("from Clerk").list();
		return li;
	}
<<<<<<< HEAD

	
	public Clerk getClerkById(int id) {
		SQLQuery query=sessionFactory.openSession().createSQLQuery("select * from clerk where clerkid=:id").addEntity(Clerk.class);
		query.setParameter("id",id);
		Clerk clerk= (Clerk) query.uniqueResult();
		return clerk;
	}

	
	public void updateClerk(Clerk clerk) {
		SQLQuery query=sessionFactory.openSession().createSQLQuery("update clerk set firstname=:name,lastname=:lname,gender=:gender,contactnumber=:number,age=:age where clerkid=:id").addEntity(Clerk.class);
		query.setParameter("name",clerk.getFirstname());
		query.setParameter("lname",clerk.getLastname());
		query.setParameter("gender", clerk.getGender());
		query.setParameter("number",clerk.getContactnumber());
		query.setParameter("age", clerk.getAge());
		query.setParameter("id",clerk.getClerkid());
		query.executeUpdate();		
	}

	
	public int deleteClerkById(int id) {
		int result=0;
		Clerk clerk=(Clerk) sessionFactory.openSession().load(Clerk.class, id);	
		if(clerk!=null) {
			Query query=sessionFactory.openSession().createQuery("delete from Clerk c where c.id=:id");
			query.setParameter("id", id);
			 result=query.executeUpdate();
			
		}
		return result;
			}


	public List<Clerk> viewClerkById(int clerkid) {
	Query query= (Query) sessionFactory.openSession().createQuery("from Clerk c where c.clerkid=?");
	query.setParameter(0,clerkid);
	List<Clerk> li=query.list();
		return li;
	}

	
	/*
	 * public void updateClerk(Clerk clerk) { sessionFactory.openSession().
	 * 
	 * }
	 */
=======
>>>>>>> 5e448212e5eb21d89de2a3d35a2c6e369401553f
	
}
