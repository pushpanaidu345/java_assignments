package com.dao;

import com.model.Patient;

public interface PatientDaoIntf {

	void savePatient(Patient patient);

}
