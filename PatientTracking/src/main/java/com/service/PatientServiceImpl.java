package com.service;

import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

 

import com.dao.PatientDaoIntf;
import com.model.Patient;
    @Service
    @Transactional
    public class PatientServiceImpl  implements PatientServiceIntf{
    @Autowired
        PatientDaoIntf dao;
        
        public void savePatient(Patient patient) {
            
            dao.savePatient(patient);
        }

 

        /*@Override
        public List<Patient> getAllDetails() {
            List<Patient> li=dao.getAllDetails();
            return li;
        }*/
        

 

}
