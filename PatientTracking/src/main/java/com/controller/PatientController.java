package com.controller;

import java.util.List;

 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

 

import com.model.Patient;
import com.service.PatientServiceIntf;

 


    @Controller
    public class PatientController {
        @Autowired
        PatientServiceIntf service;
    @RequestMapping("/patient")
    public ModelAndView registerPage(@ModelAttribute("patient") Patient patient) {
        
        return new ModelAndView("patient");
        
    }
    @RequestMapping("/savePatient")
    public ModelAndView saveRegister(@Validated @ModelAttribute("patient") Patient patient) {
        //BindingResult result) {
       /* if(result.hasErrors()) {
            return new ModelAndView("patientdetails");
        }
        else {*/
            
            service.savePatient(patient);
       //return new ModelAndView("redirect:/fetchUsers");
            return new ModelAndView("success");
        }
    /*
    @RequestMapping("/fetchUsers")
    public ModelAndView getAllDetails()
  {
      List<Patient>ls=service.getAllDetails();
      return new ModelAndView("patientdetails","listOfUsers",ls);
  }
*/
}