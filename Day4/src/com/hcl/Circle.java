package com.hcl;

public class Circle extends Shape {
	private int radius;
	float PI = 3.14f;

	public Circle(String name, int radius) {
		super(name);
		this.radius = radius;
		System.out.println("circle "+name);

	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	@Override
	public float calculateArea() {
		// TODO Auto-generated method stub
		return PI * radius * radius;
	}

}
