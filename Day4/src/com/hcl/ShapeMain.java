package com.hcl;

import java.util.Scanner;

public class ShapeMain {

	public static void main(String[] args) {
		System.out.println("enter what you what: Square , Circle, Rectangle");
		Scanner sc = new Scanner(System.in);
		String str = sc.next();
		switch (str) {
		case "Square":
			Square square = new Square("square", 10);
			System.out.println("area of square: " + square.calculateArea());
			break;
		case "Circle":
			Circle circle = new Circle("circle", 20);
			System.out.println("area of circle: " + circle.calculateArea());
			break;
		case "Rectangle":
			Rectangle rect = new Rectangle("rectangle", 10, 20);
			System.out.println("arae of rectangle: " + rect.calculateArea());
			break;
		}

	}

}
