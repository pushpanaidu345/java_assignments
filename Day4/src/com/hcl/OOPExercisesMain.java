package com.hcl;

public class OOPExercisesMain {

	public static void main(String[] args) {
        FirstClass objA = new FirstClass(); 
        SecondClass objB = new SecondClass(); 
        System.out.println("in main(): "); 
        System.out.println("objA.a = "+objA.getA()); 
        System.out.println("objB.b = "+objB.getB()); 
        objA.setA (222); 
        objB.setB (333.33); 
        System.out.println("objA.a = "+objA.getA()); 
        System.out.println("objB.b = "+objB.getB()); 

        
	}

}
