package com.abst;

/*
 * Create an abstract class named Card with the following protected attributes / member variables. 
 String holderName; 
 String cardNumber; 
 String expiryDate; 
 Include appropriate getters and setters. 
 Include appropriate constructors. In the 3-argument constructor, the order of the arguments is holderName, cardNumber, expiryDate. 
 Create a class named MembershipCard. The class MembershipCard is a derived class of Card. Include the following private attributes / member variables. 
 Integer rating 
 Include appropriate getters and setters. 
 Include appropriate constructors. In the 4-argument constructor, the order of the arguments is holderName, cardNumber, expiryDate, rating. 
 Create a class named PaybackCard. The class PaybackCard is a derived class of Card. Include the following private attributes / member variables. 
 Integer pointsEarned; 
 Double totalAmount; 
 Include appropriate getters and setters. 
 Include appropriate constructors. In the 5-argument constructor, the order of the arguments is holderName, cardNumber, expiryDate, pointsEarned, totalAmount. 
 Create another class called Main. In the method, create instances of the above classes and test the above classes. The card details are entered separated by a �|�. 
All text in bold corresponds to input and the rest corresponds to output. 
 Sample Input and Output 1: 
 Select the Card 
 1.Payback Card 
 2.Membership Card 
 1 
 Enter the Card Details: 
 Anandhi|12345|14/01/2020 
 Enter points in card 
 1000 
 Enter Amount 
 50000 
 

 */
public abstract class Card {
	protected String holderName; 
	protected String cardNumber; 
	protected String expiryDate;
	public String getHolderName() {
		return holderName;
	}
	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public Card(String holderName, String cardNumber, String expiryDate) {
		super();
		this.holderName = holderName;
		this.cardNumber = cardNumber;
		this.expiryDate = expiryDate;
	} 
	

}
