package com.main;
/*
 * . Write a Java program to swap two variables
 */
public class Swap {
	public void toSwap(int a,int b) {
		System.out.println("before swapping "+" "+"a="+a+" "+"b="+b);
		int temp=a;
		a=b;
		b=a;
		System.out.println("After swapping"+" "+"a="+a+" "+"b="+b);
		/*
		 * a=a+b; b=a-b; a=a-b;
		 */
	}

	public static void main(String[] args) {
		Swap swap=new Swap();
		swap.toSwap(10,34);

	}

}
