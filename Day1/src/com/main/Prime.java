package com.main;
/*
 * . Write a java program, which will take a number variable and check whether the number is prime or not.
Note: Prime number is a number that is greater than 1 and divided by 1 or itself only. For example, 2, 3, 5, 7, 11, 13, 17.... are the prime numbers

 */
import java.util.Scanner;

public class Prime {
	public boolean isPrime(int num) {
		int c=0;
		for(int i=1;i<=num;i++) {
			if(num%i==0) {
				c++;
			}
		}
		if(c==2)
			return true;
		else
			return false;
	}

	public static void main(String[] args) {
		Prime obj=new Prime();
		Scanner sc=new Scanner(System.in);
		System.out.println("enter a number:");
		int num=sc.nextInt();
		if(obj.isPrime(num)) 
			System.out.println("The Entered number is prime  ");
		else
			System.out.println("entered number is not prime");

	}

}
