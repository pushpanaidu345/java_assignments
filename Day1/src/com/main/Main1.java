package com.main;

import java.util.Scanner;

/*
 * Write a program to read a number, calculate the sum of squares of even digits (values) present in the given number.
Create a class UserMainCode with a static method sumOfSquaresOfEvenDigits which accepts a positive integer. The return type (integer) should be the sum of squares of the even digits.
Create a class Main which would get the input as a positive integer and call the static method sumOfSquaresOfEvenDigits present in the UserMainCode.
Sample Input 1:
56895
Sample Output 1:
100

 */
class UserMainCode1 {
	public static int sumOfSquaresOfEvenDigits(int num) {
		int sum = 0;
		while (num > 0) {
			int d = num % 10;
			if (d % 2 == 0)
				sum = sum + d * d;
			num = num / 10;
		}
		return sum;
	}
}

public class Main1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("enter a number:");
		int num = sc.nextInt();
		int res = UserMainCode1.sumOfSquaresOfEvenDigits(num);
		System.out.println(res);

	}

}
