package com.main;

import java.util.Scanner;

/*
 * Write a Java program that takes three numbers as input to 
 * calculate and print the average of the numbers.
 */
public class Average {
	public void calculateAvg(int a,int b,int c) {
		int sum=a+b+c;
		int avg=sum/3;
		System.out.println("average of 3 numbers: "+avg);
	}

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("enter three numbers:");
		int a=sc.nextInt();
		int b=sc.nextInt();
		int c=sc.nextInt();
		Average obj=new Average();
		obj.calculateAvg(a, b, c);
		

	}

}
