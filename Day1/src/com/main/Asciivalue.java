package com.main;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

/*
 * Write a Java program to print the ascii value of a given character.
 */
public class Asciivalue {

	public static void main(String[] args) {
		char ch='A';
		System.out.println("ascii value of a given character "+(int)ch);

	}

}
