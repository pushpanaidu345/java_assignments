package com.main;

import java.util.Scanner;

/*
 * Write a Program which finds the longest word from a sentence. Your program should read a sentence as input from user and return the longest word. In case there are two words of maximum length return the word which comes first in the sentence.
Include a class UserMainCode with a static method getLargestWord which accepts a string The return type is the longest word of type string.
Create a Class Main which would be used to accept two Input strings and call the static method present in UserMainCode.
Sample Input 1:
Welcome to the world of Programming
Sample Output 1:
Programming
Sample Input 2:
ABC DEF
Sample Output 2:
ABC

 */
class UserMainCode3{
	public static String getLargestWord(String str) {
		String[] strarr=str.split(" ");
		int max=0;
		String result="";
		for(int i=0;i<strarr.length;i++) {
			int len=strarr[i].length();
			if(max<len  ) {
				max=len;
				result=strarr[i];
			}
				
			
			
			
		}
		return result;
	}
}
public class Main2 {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("enter a string:");
		String str=sc.nextLine();
		System.out.println(UserMainCode3.getLargestWord(str));

	}

}
