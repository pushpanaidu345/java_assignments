package com.main;

import java.util.Scanner;

/*
 * . Write a program to read a number and calculate the sum of odd digits (values) present in the given number.
Create a class with a static method checkSum which accepts a positive integer. The return type should be 1 if the sum is odd. In case the sum is even return -1 as output.
Create a class Main which would get the input as a positive integer and call the static method checkSum present in the UserMainCode.
Sample Input 1:
56895
Sample Output 1:
Sum of odd digits is odd.
Sample Input 2:
84228
Sample Output 2:
Sum of odd digits is even.

 */
class UserMainCode {
	public static int checkSum(int num) {
		int sum = 0;
		while (num > 0) {
			int d = num % 10;
			if (d % 2 != 0)
				sum = sum + d;
			num = num / 10;
		}
		if (sum % 2 == 0)
			return -1;
		else
			return 1;
	}
}

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("enter a number:");
		int num = sc.nextInt();

		int result = UserMainCode.checkSum(num);
		if (result == -1)
			System.out.println("sum of odd digits is even");
		else
			System.out.println("sum of odd digits is odd");

	}

}
