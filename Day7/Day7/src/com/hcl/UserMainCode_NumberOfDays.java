package com.hcl;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class UserMainCode_NumberOfDays {
public static int getNumberOfDays(int year,int month) {
		
		Calendar calendar = new GregorianCalendar(year, month, 1); //used to count number of days
        int numberOfDays = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		return numberOfDays ;
	}

}
