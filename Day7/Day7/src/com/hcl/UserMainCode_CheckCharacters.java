package com.hcl;

public class UserMainCode_CheckCharacters {
	public static int checkCharacters(String string) {

		int length = string.length();
		char firstChar = string.charAt(0);
		char lastChar = string.charAt(length - 1);
		int output;
		if (firstChar == (lastChar)) {
			output = 1;

		} else
			output = 0;
		return output;

	}
}
