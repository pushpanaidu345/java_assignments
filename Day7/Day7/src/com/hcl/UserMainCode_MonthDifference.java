package com.hcl;

public class UserMainCode_MonthDifference {
	public static int getMonthDifference(String date1,String date2) {
		
		String[] arr=date1.split("-");
		int year1=Integer.valueOf(arr[0]);
		int month1=Integer.valueOf(arr[1]);
		
		String[] arr2=date2.split("-");
		int year2=Integer.valueOf(arr2[0]);
		int month2=Integer.valueOf(arr2[1]);
		
		int dif=Math.abs(year1-year2);
		int month=Math.abs(month1-month2);
		int totalMonths=(dif*12)+month;
		return totalMonths;
		
	}

}
