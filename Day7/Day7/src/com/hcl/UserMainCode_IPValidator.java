package com.hcl;

public class UserMainCode_IPValidator {
public static int ipValidator(String string) {
	String[] arr=string.split(".");
	boolean isTrue=false;
	for(int i=0;i<arr.length;i++) {
		int number=Integer.valueOf(arr[i]);
		if(number>=0 && number<=255) {
			isTrue=true;
		}
		
	}
	if(isTrue)
		return 1;
	else
		return 2;
	
}
}
