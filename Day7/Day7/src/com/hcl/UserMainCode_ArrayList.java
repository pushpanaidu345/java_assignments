package com.hcl;

import java.util.ArrayList;
import java.util.Collections;

public class UserMainCode_ArrayList {
public static ArrayList<Integer> sortMergedArrayList(ArrayList<Integer> arraylist1,ArrayList<Integer> arraylist2){
	ArrayList<Integer> arraylist3=new ArrayList<>();
	arraylist3.addAll(arraylist1);
	arraylist3.addAll(arraylist2);
	Collections.sort(arraylist3);
	ArrayList<Integer> arraylist4=new ArrayList<>();
	arraylist4.add(arraylist3.get(2));
	arraylist4.add(arraylist3.get(6));
	arraylist4.add(arraylist3.get(8));
	return arraylist4;
}
}