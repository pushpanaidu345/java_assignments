package com.hcl;

public class UserMainCode_Olddate {
	public static String findOldDate(String date1, String date2) {
		String[] arr = date1.split("-");
		int year1 = Integer.valueOf(arr[2]);

		String[] arr2 = date2.split("-");
		int year2 = Integer.valueOf(arr2[2]);
		String output = "";
		int max;
		if (year1 > year2)
			max = year1;
		else
			max = year2;
		if (max == year2)
			output = String.join("/", arr);
		else
			output = String.join("/", arr2);

		return output;
	}
}
