package com.hcl;

import java.util.ArrayList;
import java.util.List;

public class UserMainCode_StringSplitter {

	public static String[] manipulateLiteral(String string, String ch) {
		ArrayList<String> list=new ArrayList<>();
		String[] array=string.split(ch);
		for(int i=0;i<array.length;i++) {
			list.add(array[i]);
		}
		String[] output=new String[array.length];
		Object[] ele=list.toArray();
		for(int i=0;i<ele.length;i++) {
			String element=(String) ele[i];
			String elelower=element.toLowerCase();
			StringBuffer sb=new StringBuffer();
			sb.append(elelower);
			sb.reverse();
			output[i]=sb.toString();
		}
		return output;
	}

}
