package com.hcl;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class UserMainCode_DateDifference {

	public static long getDateDifference(String date1, String date2) {
		
		LocalDate dt1=LocalDate.parse(date1);
		LocalDate dt2=LocalDate.parse(date2);
		long days=ChronoUnit.DAYS.between(dt1, dt2);
		//long days=dt1.until(dt2,ChronoUnit.DAYS); //this is another way to use 
				 
		return days;
	}

}
