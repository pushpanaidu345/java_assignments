package com.main;

import java.util.Scanner;

import com.hcl.UserMainCode_IPValidator;

/*
 * Write a program to read a string and validate the IP address. Print “Valid” if the IP address is valid, else print “Invalid”. 
  
Include a class UserMainCode with a static method ipValidator which accepts a string. The return type (integer) should return 1 if it is a valid IP address else return 2. 
Create a Class Main which would be used to accept Input String and call the static method present in UserMainCode. 
 
Note: An IP address has the format a.b.c.d where a,b,c,d are numbers between 0-255. 
  
Sample Input 1: 
132.145.184.210 
Sample Output 1: 
Valid 
  
Sample Input 2: 
132.145.184.290 
Sample Output 2: 
Invalid 

 */
public class Main_IPValidator {

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		System.out.println("enter Ip address ");
		String string=input.next();
		int result=UserMainCode_IPValidator.ipValidator(string);
		if(result==1)
			System.out.println("Valid");
		else
			System.out.println("Invalid");
		}

}
