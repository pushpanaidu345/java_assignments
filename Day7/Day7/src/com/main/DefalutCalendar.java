package com.main;

import java.util.Calendar;

/*
 * Write a Java program to get and display information (year, month, day, hour, minute) of a default calendar 
 */
public class DefalutCalendar {

	public static void main(String[] args) {
		Calendar calendar=Calendar.getInstance();
		System.out.println("current Date "+calendar.getTime());
		System.out.println("year: "+calendar.get(Calendar.YEAR));
		System.out.println("Month: "+calendar.get(Calendar.MONTH));
		System.out.println("Day: "+calendar.get(Calendar.DATE));
		System.out.println("Hour: "+calendar.get(Calendar.HOUR));
		System.out.println("Minute: "+calendar.get(Calendar.MINUTE));

	}

}
