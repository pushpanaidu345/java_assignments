package com.main;

import java.util.Scanner;

import com.hcl.UserMainCode_DateDifference;

/*
 * Get two date strings as input and write code to find difference between two dates in days. 
Include a class UserMainCode with a static method getDateDifference which accepts two date strings as input. 
The return type of the output is an integer which returns the diffenece between two dates in days. 
Create a class Main which would get the input and call the static method getDateDifference present in the UserMainCode. 
  
Sample Input 1: 
2012-03-12 
2012-03-14 
Sample Output 1: 
2 
 
Sample Input 2: 
2012-04-25 
2012-04-28 
Sample Output 2: 
3 

 */
public class Main_DateDifference {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("enter two Strings:");
		String date1 = input.next();
		String date2 = input.next();
		System.out.println(UserMainCode_DateDifference.getDateDifference(date1, date2));

	}

}
