package com.main;

import java.util.Scanner;

import com.hcl.UserMainCode_MonthDifference;

/*
 * Given a method with two date strings in yyyy-mm-dd format as input. Write code to find the difference between two dates in months.  
Include a class UserMainCode with a static method getMonthDifference which accepts two date strings as input.  
The return type of the output is an integer which returns the diffenece between two dates in months.  
Create a class Main which would get the input and call the static method getMonthDifference present in the UserMainCode. 
  
Sample Input 1: 
2012-03-01 
2012-04-16 
Sample Output 1: 
1 
Sample Input 2: 
2011-03-01 
2012-04-16 
Sample Output 2: 
13 

 */
public class Main_MonthDifference {

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		System.out.println("enter two dates");
		String date1=input.next();
		String date2=input.next();
		System.out.println(UserMainCode_MonthDifference.getMonthDifference(date1, date2));
		}

}
