package com.main;

import java.util.Scanner;

import com.hcl.UserMainCode_DateFormat;

/*
 * Given a date string in the format dd/mm/yyyy, write a program to convert the given date to the format dd-mm-yy. 
Include a class UserMainCode with a static method “convertDateFormat” that accepts a String and returns a String. 
 Create a class Main which would get a String as input and call the static method convertDateFormat present in the UserMainCode. 
Sample Input: 
12/11/1998 
  
Sample Output: 
12-11-1998 

 */
public class Main_DateFormat {

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		System.out.println("enter date");
		String date=input.next();
		System.out.println(UserMainCode_DateFormat.convertDateFormat(date));
		
	}

}
