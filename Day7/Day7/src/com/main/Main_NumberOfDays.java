package com.main;

import java.util.Scanner;

import com.hcl.UserMainCode_NumberOfDays;

/*
 * .Include a class UserMainCode with a static method �getNumberOfDays� that accepts 2 integers as arguments and returns an integer. The first argument corresponds to the year and the second argument corresponds to the month code. The method returns an integer corresponding to the number of days in the month. 
Create a class Main which would get 2 integers as input and call the static method getNumberOfDays present in the UserMainCode. 
Input and Output Format: 
Input consists of 2 integers that correspond to the year and month code. 
Output consists of an integer that correspond to the number of days in the month in the given year. 
Sample Input: 
2000 
1 
Sample Output: 
29 

 */
public class Main_NumberOfDays {

	public static void main(String[] args) {
		Scanner input =new Scanner(System.in);
		System.out.println("enter a year:");
		int year=input.nextInt();
		System.out.println("enter a month in digits as jan=0,feb=1,...");
		int month=input.nextInt();
		int countDays=UserMainCode_NumberOfDays.getNumberOfDays(year,month);
		System.out.println("total number of days in the given month for the given year "+countDays);

	}

}
