package com.main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import com.hcl.UserMain_EmployeeHashMap;

/*
 * A Company wants to obtain employees of a particular designation. You have been assigned as the programmer to build this package. You would like to showcase your skills by creating a quick prototype. The prototype consists of the following steps: 
    Read Employee details from the User. The details would include name and designaton in the given order. The datatype for name and designation is string. 
    Build a hashmap which contains the name as key and designation as value. 
    You decide to write a function obtainDesignation which takes the hashmap and designation as input and returns a string array of employee names who belong to that designation as output. Include this function in class UserMainCode. 
Create a Class Main which would be used to read employee details in step 1 and build the hashmap. Call the static method present in UserMainCode. 
Sample Input 1: 
4 
Manish 
MGR 
Babu 
CLK 
Rohit 
MGR 
Viru 
PGR 
MGR 
 
Sample Output 1: 
Manish 
Rohit 

 */
public class Main_EmployeeHashMap {

	public static void main(String[] args) {
		Scanner in=new Scanner(System.in);
		System.out.println("number of employees:");
		int number=in.nextInt();
		HashMap<String,String> map=new HashMap<>();
		for(int i=0;i<number;i++) {
			System.out.println("enter name:");
			
			String name=in.next();
			System.out.println("enter designation:");
			String designation=in.next();
			map.put(name, designation);
		}
		System.out.println("enter designation you to know names:");
		String userdesignation=in.next();
		System.out.println(UserMain_EmployeeHashMap.obtainDesignation(map,userdesignation));
		
		}
		}
		


