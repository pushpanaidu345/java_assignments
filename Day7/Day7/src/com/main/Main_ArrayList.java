package com.main;

import java.util.ArrayList;
import java.util.Scanner;

import com.hcl.UserMainCode_ArrayList;

/*
 * Write a code to read two int array lists of size 5 each as input and to merge the two arrayLists, sort the merged arraylist in ascending order and fetch the elements at 2nd, 6th and 8th index into a new arrayList and return the final ArrayList. 
 Include a class UserMainCode with a static method sortMergedArrayList which accepts 2 ArrayLists. 
 The return type is an ArrayList with elements from 2,6 and 8th index position .Array index starts from position 0. 
 Create a Main class which gets two array list of size 5 as input and call the static method sortMergedArrayList present in the UserMainCode. 
Sample Input: 
3 
1 
17 
11 
19 
5 
2 
7 
6 
20 
Sample Output: 
3 
11 
19 
Sample Input : 
1 
2 
3 
4 
5 
6 
7 
8 
9 
10 
Sample Output : 
3 
7 
9 
 

 */
public class Main_ArrayList {

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		ArrayList<Integer> arraylist1=new ArrayList<>();
		ArrayList<Integer> arraylist2=new ArrayList<>();
		System.out.println("enter 5 arraylist1 elements:");
		for(int i=0;i<5;i++) {
			int number=input.nextInt();
			arraylist1.add(number);
		}
		System.out.println("enter 5 arraylist2 elements:");
		for(int i=0;i<5;i++) {
			int number=input.nextInt();
			arraylist2.add(number);
		}
		
		
				System.out.println(UserMainCode_ArrayList.sortMergedArrayList(arraylist1,arraylist2));
	}

}
