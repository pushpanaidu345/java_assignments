package com.main;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;

/*
 * Given two inputs year and month (Month is coded as: Jan=0, Feb=1 ,Mar=2 ...), 
 * write a program to find out total number of days in the given month for the given year. 
 */
class Days{
	public static int toCountDays(int year,int month) {
		
		Calendar calendar = new GregorianCalendar(year, month, 1); //used to count number of days
        int numberOfDays = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		return numberOfDays ;
	}
}
public class CountDays {

	public static void main(String[] args) {
		Scanner input =new Scanner(System.in);
		System.out.println("enter a year:");
		int year=input.nextInt();
		System.out.println("enter a month in digits as jan=0,feb=1,...");
		int month=input.nextInt();
		int countDays=Days.toCountDays(year,month);
		System.out.println("total number of days in the given month for the given year "+countDays);

	}

	

}
