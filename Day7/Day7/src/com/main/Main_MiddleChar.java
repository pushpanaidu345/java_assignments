package com.main;

import java.util.Scanner;

import com.hcl.UserMainCode_MiddleChar;

/*
 * .    Fetching Middle Characters from String
 
Write a program to read a string of even length and to fetch two middle most characters from the input string and return it as string output.
 
Include a class UserMainCode with a static method getMiddleChars which accepts a string of even length as input . The return type is a string which should be the middle characters of the string.
 
Create a class Main which would get the input as a string and call the static method getMiddleCharspresent in the UserMainCode.
 
Input and Output Format:
Input consists of a string of even length.
Output is a string .
Refer sample output for formatting specifications.
 
Sample Input 1:
this
Sample Output 1:
hi
 
Sample Input 1:
Hell
Sample Output 1:
el

 */
public class Main_MiddleChar {

	public static void main(String[] args) {
		Scanner input =new Scanner(System.in);
		System.out.println("Enter even length String only: ");
		String string=input.next();
		String result=UserMainCode_MiddleChar.getMiddleChars(string);
		System.out.println("middle char in the given string "+result);

	}

}
