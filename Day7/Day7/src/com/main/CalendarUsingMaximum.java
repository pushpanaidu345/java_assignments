package com.main;

import java.util.Calendar;

/*
 * Write a Java program to get the maximum value of the year, month, week, date 
 * from the current date of a default calendar 
 */
public class CalendarUsingMaximum {

	public static void main(String[] args) {
		Calendar calendar=Calendar.getInstance();
		System.out.println("maximum value of the year "+calendar.getMaximum(Calendar.YEAR));
		System.out.println("maximum value of the month "+calendar.getMaximum(Calendar.MONTH));
		System.out.println("maximum value of the week "+calendar.getMaximum(Calendar.DAY_OF_WEEK));
		System.out.println("maximum value of the date "+calendar.getMaximum(Calendar.DATE));

	}

}
