package com.main;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/*
 * .Write a java program to print current date and time in the specified format. 
 */
public class Currentdate {

	public static void main(String[] args) {
		LocalDateTime datetime=LocalDateTime.now(); //This class is from java8 feature
		//Date date=new Date();   //use Date Object 
		System.out.println("current date and time: "+datetime);
		DateTimeFormatter format=DateTimeFormatter.ofPattern("dd-mm-yyyy  hh:mm:ss");
		String date=datetime.format(format);
		System.out.println("current date and time in the specified format "+date);
		}

}
