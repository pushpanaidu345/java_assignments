package com.main;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/*
 * Write a Java program to extract date, time from the date string 
 */
public class ExtractDateTime {

	//@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		LocalDateTime date=LocalDateTime.now();
		//Date date=new Date();
		DateTimeFormatter format=DateTimeFormatter.ofPattern("dd-mm-yyyy hh:mm:ss");
		String datetimeformat =date.format(format);
		String[] datetime=datetimeformat.split(" ");
		System.out.println("date: "+datetime[0]);
		System.out.println("Time: "+datetime[1]);
		

	}

}
