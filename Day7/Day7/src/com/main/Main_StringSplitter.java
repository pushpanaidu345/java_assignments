package com.main;

import java.util.List;
import java.util.Scanner;
import java.util.Set;

import com.hcl.UserMainCode_StringSplitter;

/*
 * Write a program which would accept a string and a character as a delimiter. Apply the below rules

1. Using the delimiter, split the string and store these elements in array.
2. Reverse each element of the string and convert it into lowercase.

Include a class UserMainCode with a static method manipulateLiteral which accepts the string and character. The return type is the string array formed.

Create a Class Main which would be used to accept the string and characterand call the static method present in UserMainCode.

Input and Output Format:

Input consists of a string and character.
Output consists of a string array.

Refer sample output for formatting specifications.

Sample Input 1:
AAA/bba/ccc/DDD
/

Sample Output 1:
aaa
abb
ccc
ddd

 */
public class Main_StringSplitter {

	public static void main(String[] args) {
		Scanner input =new Scanner(System.in);
		System.out.println("enter strings each string separated by delimiter:");
		String string=input.next();
		System.out.println("enetr delimiter: ");
		String ch=input.next();
		String[] result=UserMainCode_StringSplitter.manipulateLiteral(string,ch);
		for(int i=0;i<result.length;i++) {
			System.out.println(result[i]);
		}
	}

}
