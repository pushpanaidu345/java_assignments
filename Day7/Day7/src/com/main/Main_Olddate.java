package com.main;

import java.util.Scanner;

import com.hcl.UserMainCode_MonthDifference;
import com.hcl.UserMainCode_Olddate;

/*
 * Write a program to read  two String variables in DD-MM-YYYY.Compare the two dates and return the older date in 'MM/DD/YYYY' format. 
Include a class UserMainCode with a static method findOldDate which accepts the string values. The return type is the string. 
Create a Class Main which would be used to accept the two string values and call the static method present in UserMainCode. 
Sample Input 1: 
05-12-1987 
8-11-2010 
Sample Output 1: 
12/05/1987 

 */
public class Main_Olddate {

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		System.out.println("enter two dates");
		String date1=input.next();
		String date2=input.next();
		System.out.println(UserMainCode_Olddate.findOldDate(date1, date2));
		}
	}


