package com.main;

import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

import com.hcl.UserMainCode_SortedArray;

/*
 * Write a program to read a string array, remove duplicate elements and sort the array.
Note:
1.	    The check for duplicate elements must be case-sensitive. (AA and aa are NOT duplicates)
2.	    While sorting, words starting with upper case letters takes precedence.

Include a class UserMainCode with a static method orderElements which accepts the string array. The return type is the sorted array.

Create a Class Main which would be used to accept the string arrayand integer and call the static method present in UserMainCode.

Input and Output Format:

Input consists of an integer n which is the number of elements followed by n string values.

Output consists of the elements of string array.

Refer sample output for formatting specifications.

Sample Input 1:
6
AAA
BBB
AAA
AAA
CCC
CCC

Sample Output 1:
AAA
BBB
CCC


Sample Input 2:
7
AAA
BBB
aaa
AAA
Abc
A
b

Sample Output 2:
A
AAA
Abc
BBB
aaa
b

 */
public class Main_SortedArray {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("enter number of elements:");
		int number = input.nextInt();
		System.out.println("enter elements in string array:");
		String[] elements = new String[number];
		for (int i = 0; i < number; i++) {
			elements[i] = input.next();
		}
		Set set1 = (UserMainCode_SortedArray.orderElements(elements, number));
		Iterator iterator = set1.iterator();
		while (iterator.hasNext()) {
			System.out.println(iterator.next());
		}
	}

}
