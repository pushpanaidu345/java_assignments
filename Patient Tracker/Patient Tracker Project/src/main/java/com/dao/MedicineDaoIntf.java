package com.dao;

import java.util.List;

import com.model.Medicine;




public interface MedicineDaoIntf {
	public void saveMedicine(Medicine medicine);
	public List<Medicine> medicineDetails();
	public int deleteMedicineById(int medicineid);
	public List<Medicine> viewMedicineById(int medicineid);
	Medicine getMedicineById(int medicineid);
	void updateMedicine(Medicine medicine);
}
