package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dao.DoctorDaoIntf;
import com.model.Clerk;
import com.model.Doctor;
@Service
@Transactional
public class DoctorServiceImpl implements DoctorServiceIntf{

	
	@Autowired
	DoctorDaoIntf dao;
	public void saveDoctor(Doctor doctor) {
		dao.saveDoctor(doctor);
		
	}
	
	public List<Doctor> doctorDetails() {
		 List<Doctor>li= dao.doctorDetails();
		 return li;
		
	}

	
	
	
	public void saveEditDoctor(Doctor doctor) {
		dao.saveEditDoctor(doctor) ;
		
	}

	
	public int deleteDoctorById(int id) {
				return dao.deleteDoctorById(id);
	}

	
	public List<Doctor> viewDoctorById(int doctorid) {
		List<Doctor> li=dao.viewDoctorById(doctorid);
		return li;
	}

	
	public Doctor getDoctorById(int id) {
		
		return dao.getDoctorById(id);
	}

	
	public void updateDoctor(Doctor doctor) {
		dao.updateDoctor(doctor);
		
	}

	
}
