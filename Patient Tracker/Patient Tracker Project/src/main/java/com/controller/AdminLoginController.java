package com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.model.AdminRegister;
import com.service.AdminRegisterServiceIntf;

import com.service.AdminRegisterServiceIntf;
@Controller
public class AdminLoginController {
    @Autowired
    AdminRegisterServiceIntf service;
    @RequestMapping("/AdminLogin")
    public ModelAndView adminLogin() {
        return new ModelAndView("loginform");
        
    }
    @RequestMapping(value="/saveLogin",method=RequestMethod.POST)
    public ModelAndView checkAdmin(@RequestParam("adminid") int  adminid,@RequestParam("password") String password,AdminRegister register )
        {
           ModelAndView model=new ModelAndView(); 
        boolean status=    service.checkAdmin(adminid,password);
        if(status)
        {
        	model.addObject("name", register.getFirstname());
            return new ModelAndView("homepage");
        }
        else
        {
           model.addObject("error","Invalid id or password");
            return new ModelAndView("redirect:/AdminLogin");
            
        }
        }
        @RequestMapping("/homepage")
        public ModelAndView homePage() {
			return new ModelAndView("homepage");
        	
        }
        @RequestMapping("/forgotpassword")
        public ModelAndView forgotPassword() {
        	
			return new ModelAndView("password");
        	
        }
        @RequestMapping(value="/savepassword",method=RequestMethod.POST)
        public ModelAndView savePassword(@RequestParam("adminid") int adminId,@RequestParam("password") String password,Model model) {
        	service.forgotPassword(adminId,password);
        	model.addAttribute("status", "password changed successfully");
			return new ModelAndView("redirect:/AdminLogin");
        	
        }
        

}
