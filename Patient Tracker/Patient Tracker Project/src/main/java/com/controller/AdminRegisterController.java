package com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.model.AdminRegister;
import com.service.AdminRegisterServiceIntf;

@Controller
public class AdminRegisterController {
	@Autowired
	AdminRegisterServiceIntf service;
	@RequestMapping("/register")
	public ModelAndView adminRegister(@ModelAttribute("adminregister") AdminRegister register) {
		return new ModelAndView("registerform");
		
	}
	@RequestMapping(value="/saveRegister",method=RequestMethod.POST)
	public ModelAndView saveRegister(@Validated @ModelAttribute("adminregister") AdminRegister register,BindingResult result, Model model) {
		ModelAndView m=new ModelAndView();
		if(result.hasErrors()) {
			return new ModelAndView("registerform");
		}
		else {
		service.saveAdminRegister(register);
		//String name=register.getFirstname();
		//int id=register.getAdminid();
		model.addAttribute("admin", register);
		m.addObject("sucessregister", "You Are Registered Successsfully");
	return new ModelAndView("redirect:/AdminLogin");}
		
	}
}


