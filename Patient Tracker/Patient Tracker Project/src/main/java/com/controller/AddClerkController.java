package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import com.model.Clerk;
import com.service.ClerkServiceIntf;
@Controller
public class AddClerkController {
	@Autowired
	ClerkServiceIntf service;
	@RequestMapping("/addClerk")
	public ModelAndView addClerk( @ModelAttribute("clerk") Clerk clerk) {
		return new ModelAndView("addclerkform");
		
	}
	@RequestMapping(value="/saveClerk",method=RequestMethod.POST)
	public ModelAndView saveClerk(@Validated @ModelAttribute("clerk") Clerk clerk,BindingResult result) {
		if(result.hasErrors()) {
			return new ModelAndView("addclerkform");
		}
		else {
		service.saveClerk(clerk);
		return new ModelAndView("redirect:/viewClerkDetails");
		}
	}
	@RequestMapping("/viewClerkDetails")
	public ModelAndView clerkDetails() {
		List<Clerk> li=service.clerkDetails();
		 ModelAndView model = new ModelAndView("viewclerkdetails");
	     model.addObject("clerkdetails", li);
	     return model;
	}
	@RequestMapping(value="/editClerk/{id}")
	public ModelAndView editClerk(@PathVariable int id,Model model) {
		Clerk clerk=service.getClerkById(id);
		model.addAttribute("command", clerk);
		return new ModelAndView("clerkeditform");
	}
	@RequestMapping(value="/editSaveClerk",method=RequestMethod.POST)
	public ModelAndView editClerkSave(Clerk clerk) {
		service.updateClerk(clerk);
		return new ModelAndView("redirect:/viewClerkDetails");
	}
	@RequestMapping("/deleteClerk/{id}")
	public ModelAndView deleteDoctor(@PathVariable("id") int id,Model model) {
	int result=	service.deleteClerkById(id);
	
		model.addAttribute("result","deleted successfully");
		return new ModelAndView("redirect:/viewClerkDetails");
	
}
	@RequestMapping("/searchClerkById")
	public ModelAndView searchClerkById() {
		return new ModelAndView("searchClerkId");
		
	}
	@RequestMapping(value="/searchClerkId",method=RequestMethod.POST)
	public ModelAndView viewClerkById(@RequestParam("clerkid") int clerkid) {
	List<Clerk> clerk=service.viewClerkById(clerkid);
	 ModelAndView model = new ModelAndView("viewClerkId");
     model.addObject("view", clerk);
     return model;
	
		
		
	}
}
