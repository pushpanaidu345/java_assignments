package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.model.Clerk;
import com.model.Doctor;
import com.service.ClerkServiceIntf;
import com.service.DoctorServiceIntf;
@Controller
public class DoctorController {
	@Autowired
	DoctorServiceIntf service;
	@RequestMapping("/addDoctor")
	public ModelAndView addClerk( @ModelAttribute("doctor") Doctor doctor) {
		return new ModelAndView("adddoctorform");
		
	}
	@RequestMapping(value="/saveDoctor",method=RequestMethod.POST)
	public ModelAndView saveDoctor(@Validated @ModelAttribute("doctor") Doctor doctor,BindingResult result) {
		if(result.hasErrors()) {
			return new ModelAndView("adddoctorform");
		}
		else {
		service.saveDoctor(doctor);
		return new ModelAndView("redirect:/viewDoctorDetails");
		}
	}
	@RequestMapping("/viewDoctorDetails")
	public ModelAndView doctorDetails() {
		List<Doctor> li=service.doctorDetails();
		 ModelAndView model = new ModelAndView("viewdoctordetails");
	     model.addObject("doctordetails", li);
	     return model;
	}
	@RequestMapping(value="/editDoctor/{id}")
	public ModelAndView editDoctor(@PathVariable int id,Model model) {
		Doctor doctor=service.getDoctorById(id);
		model.addAttribute("command", doctor);
		return new ModelAndView("doctoreditform");
	}
	@RequestMapping(value="/editSaveDoctor",method=RequestMethod.POST)
	public ModelAndView editDoctorSave(Doctor doctor) {
		service.updateDoctor(doctor);
		return new ModelAndView("redirect:/viewDoctorDetails");
	}
	 
@RequestMapping("/deleteDoctor/{id}")
public ModelAndView deleteDoctor(@PathVariable("id") int id,Model model) {
int result=	service.deleteDoctorById(id);

	model.addAttribute("result","deleted successfully");
	return new ModelAndView("redirect:/viewDoctorDetails");
}
@RequestMapping("/searchDoctorById")
public ModelAndView searchClerkById() {
	return new ModelAndView("searchDoctorId");
	
}
@RequestMapping(value="/searchDoctorId",method=RequestMethod.POST)
public ModelAndView viewClerkById(@RequestParam("id") int doctorid) {
List<Doctor> doctor=service.viewDoctorById(doctorid);
 ModelAndView model = new ModelAndView("viewDoctorId");
 /*if(doctorid!=0) {*/
 model.addObject("viewDoctor", doctor);
 
 return model;

	
	
}
		

}
