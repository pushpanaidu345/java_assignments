package com.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name="Patient_details")
public class Patient {
	 @Id
	 @GeneratedValue(strategy=GenerationType.IDENTITY)
	 private int patientid;
	 /*@OneToMany(cascade=CascadeType.ALL)
	 @JoinColumn(name="patientid")
	 @JoinTable(name="joinedtable",joinColumns=@JoinColumn(name="patientid"),inverseJoinColumns=@JoinColumn(name="medicineid"))*/
	 @NotEmpty(message="Disease is required")
	 private String disease;
	@NotEmpty(message="Firstname is required")
	 private String firstname;
	@NotEmpty(message="Lastname is required")
	  private String lastname;
	  private int age;
	 @NotEmpty(message="Gender is required")
	  private String gender;
	 private int medicineid;
		/*
		 * @NotEmpty(message="Address is required") private String address;
		 */
	 @Size(min=10,max=10,message="phone number should be 10 digits")
	  private String mobileno;
	
	 public String getDisease() {
		return disease;
	}
	public void setDisease(String disease) {
		this.disease = disease;
	}

    public int getPatientid() {
	return patientid;
      }
     public void setPatientid(int patientid) {
	this.patientid = patientid;
     }
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}

	/*
	 * public String getAddress() { return address; } public void setAddress(String
	 * address) { this.address = address; }
	 */
	public String getMobileno() {
		return mobileno;
	}
	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	
	}
	public int getMedicineid() {
		return medicineid;
	}
	public void setMedicineid(int medicineid) {
		this.medicineid = medicineid;
	} 
	

}
