<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Patient Tracking</title>
<style type="text/css">
.view{���
border-collapse:collapse;
background-color:cyan;
}
.error{
color:red;
}
body{
background-image:url("C:/Images/patient.jpg");

}��
</style>
</head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>


<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<body>
<nav class="navbar navbar-expand-md bg-dark navbar-dark">
  <a class="navbar-brand" href="#">Add Patient</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>


 <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav">
     <li class="nav-item">
        <a class="nav-link" href="homepage">Home</a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="viewClerkDetails">View Clerk Details</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="addClerk">addClerk</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="viewDoctorDetails">viewDoctorDetails</a>
      </li> 
<li class="nav-item">
        <a class="nav-link" href="addDoctor">addDoctor</a>
      </li> 
          
     <li class="nav-item">
        <a class="nav-link" href="patientdetails">View patient Details</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="patient">Add patient</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="medicinedetails">view Medicine Details</a>
      </li> 
     <li class="nav-item">
        <a class="nav-link" href="medicine">Add Medicine</a>
      </li>  
      <li class="nav-item">
        <a class="nav-link" href="prescription">Add prescription</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="prescriptiondetails">view Prescription Details</a>
      </li>  
       <li class="nav-item">
        <a class="nav-link" href="createBill">Create Bill</a>
      </li> 
       <br><br> 
        
<li class="nav-item">
        <a class="nav-link" href="logout">Log Out</a>
      </li>      
    </ul>
  </div>  
</nav>
<div align="right"><a href="searchPatientById">searchPatientById</a></div>
<div align="center" class="body">
<h3 style="color:tomato">Add Patient</h3>
<table border="1" class="view">
<form:form action="savePatient" modelAttribute="patient">
<tr><th>Disease</th><td>
<form:input path="disease"/><form:errors cssClass="error" path="disease"/></td></tr>
<tr><th>First name</th>
<td><form:input path="firstname"/><form:errors cssClass="error" path="firstname"/></td></tr>
<tr><th>Last name</th><td>
<form:input path="lastname"/><form:errors cssClass="error" path="lastname"/></td></tr>
<tr><th>Age</th><td>
<input type="text" name="age"/></td></tr>
<tr><th>Gender</th><td>
Male:<form:radiobutton path="gender" value="Male"/>
Female:<form:radiobutton path="gender" value="Female"/><form:errors cssClass="error" path="gender"/></td></tr>
<%-- <tr><th>Address</th><td>
<textarea cols="10" rows="2"></textarea><form:errors cssClass="error" path="address"/></td></tr> --%>
<tr><th>Mobile No</th><td>
<form:input path="mobileno"/><form:errors cssClass="error" path="mobileno"/></td></tr>
<tr><th>medicine Id</th><td>
<form:input path="medicineid"/></td>
<tr><th><td><input type="submit" value="Add patient" class="btn btn-primary">
<input type="reset" value="Cancel" class="btn btn-danger"></td>

</form:form>
</table>
</div>
</body>
</html>