<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
      <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Patient Tracking</title>
<style>
body {font-family: Arial, Helvetica, sans-serif;}
form {border: 3px solid #f1f1f1;}

input[type=text], input[type=password] {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  box-sizing: border-box;
}

button {
  background-color: #04AA6D;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 100%;
}

button:hover {
  opacity: 0.8;
}

.cancelbtn {
  width: auto;
  padding: 10px 18px;
  background-color: #f44336;
}

.imgcontainer {
  text-align: center;
  margin: 24px 0 12px 0;
}

img.avatar {
  width: 40%;
  border-radius: 50%;
}

.container {
  padding: 16px;
}

span.psw {
  float: right;
  padding-top: 16px;
}


@media screen and (max-width: 300px) {
  span.psw {
     display: block;
     float: none;
  }
  .cancelbtn {
     width: 100%;
  }
}
</style>
</head>
<body>
<c:if test="${log}!=null"> 
<c:out value="${log}"></c:out>
</c:if>
<c:if test="${error}!=null"> 
<c:out value="${error}"></c:out>
</c:if>
<c:if test="${sucessregister}!=null"> 
<c:out value="${sucessregister}"></c:out>
</c:if>
<form action="saveLogin" method="post">
<!-- AdminId:<input type="text" name="adminid"/><br>
Password<input type="password" name="password"/> -->
 <div class="imgcontainer">
     <!--  <img src="/images/admin.jpg" alt="Avatar" class="avatar">   -->
  <%-- <img src="<c:url value='/resources/images/admin.jpg'/>" alt="Avatar" class="avatar"> --%>
  <img src="C:/Images/admin.jpg" >
  </div>

  <div class="container" >
    <label for="AdminId"><b>AdminId</b></label>
    <input type="text" placeholder="Enter AdminId" name="adminid" required>

    <label for="password"><b>Password</b></label>
    <input type="password" placeholder="Enter Password" name="password" required>
        
    <button type="submit">Login</button>
    
    
  </div>

  <div class="container" style="background-color:#f1f1f1">
    <input type="reset" class="cancelbtn" value="Cancel">
    <span class="psw">Forgot <a href="forgotpassword">password?</a></span> 
  </div>

</form>

</body>
</html>