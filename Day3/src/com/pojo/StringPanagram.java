package com.pojo;

/*
 * check given string is panagram or not
 * panagrams are words or sentences containing every letter of the alphabet atleast once
 */
public class StringPanagram {

	public static boolean checkPanagram(String str) {
		boolean[] b = new boolean[26];

		int ind = 0;

		for (int i = 0; i < str.length(); i++) {
			if ('A' <= str.charAt(i) && str.charAt(i) <= 'Z')
				ind = str.charAt(i) - 'A';

			else if ('a' <= str.charAt(i) && str.charAt(i) <= 'z')

				ind = str.charAt(i) - 'a';

			else
				continue;
			b[ind] = true;
		}

		for (int i = 0; i <= 25; i++)
			if (b[i] == false)
				return (false);

		return (true);
	}

}
