package com.pojo;

import java.util.Arrays;
import java.util.Scanner;

/*
 * write a java program to sort an integer array
 * of 10 elements in ascending
 */
public class StringSort {

	public void toSort() {
		int[] input = new int[10];
		Scanner input1 = new Scanner(System.in);//take input from user
		System.out.println("enter 10 integer numbers:");
		for (int i = 0; i < 10; i++) {  //iterating loop
			input[i] = input1.nextInt();
		}
		Arrays.parallelSort(input);//parallelsort is the method present in Arrays(java8 feature)
		System.out.println("sorted elements");
		for (int i : input) {      //Enhanced for loop
			System.out.println(i);
		}
	}
}
