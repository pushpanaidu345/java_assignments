package com.pojo;

/*
 * search for an element of an integer array of 10 elements
 */
public class ElementSearch {
	public String findElement(int[] arrayinput, int searchelement) {
		boolean isFound = false;
		for (int i = 0; i < arrayinput.length; i++) {
			if (arrayinput[i] == searchelement)
				isFound = true;
			break;
		}
		if (isFound)
			return "element is found";
		else
			return "element is not found";
	}

}
