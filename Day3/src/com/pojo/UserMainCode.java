package com.pojo;
/*
 * return the String without the first 2 characters except when
 * 1.keep the first char if it is 'k'
 * 2.keep the second char if it is 'b'
 * 
 * UserMain is the main method to this program.
 */
public class UserMainCode {
	public static String getString(String input) {
		input = input.toLowerCase();
		String output = "";
		if (input.charAt(0) == 'k') {

			output = output + 'k';
		}
		if (input.charAt(1) == 'b') {
			output = output + 'b';
		} else {
			output = output + input.substring(2, input.length());
		}

		return output;
	}

}
