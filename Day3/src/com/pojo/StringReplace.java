package com.pojo;

/*
 * Write a java program to replace all the 
 * 'd' occurrence characters with 'h' characters in each string
 */
public class StringReplace {

	public String toReplace(String input) {
		String output = "";
		for (int i = 0; i < input.length(); i++) {
			if (input.charAt(i) == 'd') {
				output = output + 'h';
			} else
				output = output + input.charAt(i);
		}
		return output;
	}
}
