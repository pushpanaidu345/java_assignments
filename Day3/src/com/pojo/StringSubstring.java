package com.pojo;
/*
 * Accept a string, and two indices(integers),
 * and print the substring consisting of all characters inclusive
 * range from.. to..
 */

public class StringSubstring {
	public String toSubstring(String input, int from, int to) {
		String output = "";
		output = input.substring(from, to);// substring method
		return output;
	}

}
