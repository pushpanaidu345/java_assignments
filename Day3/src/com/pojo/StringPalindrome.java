package com.pojo;
/*
 * Given a string ,print 'Yes'
 * if it is a palindrome ,
 * print 'No' otherwise
 */

public class StringPalindrome {
	public String checkPalindrome(String input) {
		int flag = 0;
		for (int i = 0; i < input.length() / 2; i++) {
			if (input.charAt(i) == (input.charAt(input.length() - i-1))) {
				flag = 1;
			}
		}
		if (flag == 1)
			return "Yes";
		else
			return "No";
	}

}
