package com.pojo;

/*
 * Write a program to convert all the characters in  a string to lowercase
 */
public class StringLower {
	public String toLowerCase(String input)// method for convert string into lowercase
	{
		String output = "";
		output = input.toLowerCase();
		return output;
	}

}
