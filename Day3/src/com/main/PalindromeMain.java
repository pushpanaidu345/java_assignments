package com.main;

import java.util.Scanner;

import com.pojo.StringPalindrome;

public class PalindromeMain {

	public static void main(String[] args) {
		StringPalindrome palindrome = new StringPalindrome();
		Scanner sc=new Scanner(System.in);
		System.out.println("enter string:");
		String str=sc.next();
		//System.out.println(palindrome.checkPalindrome("pushpa"));
		System.out.println(palindrome.checkPalindrome(str));
	}

}
