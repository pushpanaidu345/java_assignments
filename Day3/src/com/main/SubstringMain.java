package com.main;

import java.util.Scanner;

import com.pojo.StringSubstring;

public class SubstringMain {

	public static void main(String[] args) {
		StringSubstring substring = new StringSubstring();
		Scanner sc=new Scanner(System.in);
		System.out.println("enter string:");
		String str=sc.next();
		System.out.println("enter from index");
		int from=sc.nextInt();
		System.out.println("enter to index");
		int to=sc.nextInt();
		String output1 = substring.toSubstring(str, from, to);
		System.out.println("substring of given input:  " + output1);
	}

}
